<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180110151439 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE action (id INT AUTO_INCREMENT NOT NULL, description VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE activity (id INT AUTO_INCREMENT NOT NULL, description VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE allies (id INT AUTO_INCREMENT NOT NULL, company VARCHAR(150) NOT NULL, personOfContact VARCHAR(255) NOT NULL, phone VARCHAR(100) DEFAULT NULL, email VARCHAR(100) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE implementation_guide (id INT AUTO_INCREMENT NOT NULL, startDate DATE NOT NULL, endDate DATE NOT NULL, personInCharge VARCHAR(100) NOT NULL, email VARCHAR(100) NOT NULL, author INT NOT NULL, status INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE line (id INT AUTO_INCREMENT NOT NULL, description VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE objetives (id INT AUTO_INCREMENT NOT NULL, `lines` INT DEFAULT NULL, title VARCHAR(255) NOT NULL, image VARCHAR(150) NOT NULL, INDEX IDX_4B64255F4F018C96 (`lines`), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE principles (id INT AUTO_INCREMENT NOT NULL, description VARCHAR(255) NOT NULL, icon VARCHAR(150) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE resource (id INT AUTO_INCREMENT NOT NULL, title VARCHAR(200) NOT NULL, link VARCHAR(255) NOT NULL, category INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE strategic_line (id INT AUTO_INCREMENT NOT NULL, strategicLine INT NOT NULL, Action INT NOT NULL, Activities INT NOT NULL, startDate DATE NOT NULL, endDate VARCHAR(255) NOT NULL, VerificationSources VARCHAR(255) NOT NULL, Media VARCHAR(255) NOT NULL, budget VARCHAR(50) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE target_audience (id INT AUTO_INCREMENT NOT NULL, audience VARCHAR(50) NOT NULL, sector VARCHAR(150) NOT NULL, numberOfMens INT NOT NULL, numberOfWomens INT NOT NULL, minAgeRange INT NOT NULL, maxAgeRange INT NOT NULL, profile VARCHAR(255) NOT NULL, communicationSkills VARCHAR(255) NOT NULL, VIHRiskLevel INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE task (id INT AUTO_INCREMENT NOT NULL, description VARCHAR(255) NOT NULL, comment VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE team (id INT AUTO_INCREMENT NOT NULL, job INT NOT NULL, fullName VARCHAR(150) NOT NULL, phone VARCHAR(30) DEFAULT NULL, email VARCHAR(50) DEFAULT NULL, Skype VARCHAR(50) DEFAULT NULL, ExperienceInHIV VARCHAR(255) DEFAULT NULL, formtiveNeeds VARCHAR(255) NOT NULL, supportTeam VARCHAR(255) NOT NULL, phrase VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user (id INT AUTO_INCREMENT NOT NULL, username VARCHAR(100) NOT NULL, password VARCHAR(60) NOT NULL, firstName VARCHAR(50) NOT NULL, lastName VARCHAR(50) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE objetives ADD CONSTRAINT FK_4B64255F4F018C96 FOREIGN KEY (`lines`) REFERENCES line (id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE objetives DROP FOREIGN KEY FK_4B64255F4F018C96');
        $this->addSql('DROP TABLE action');
        $this->addSql('DROP TABLE activity');
        $this->addSql('DROP TABLE allies');
        $this->addSql('DROP TABLE implementation_guide');
        $this->addSql('DROP TABLE line');
        $this->addSql('DROP TABLE objetives');
        $this->addSql('DROP TABLE principles');
        $this->addSql('DROP TABLE resource');
        $this->addSql('DROP TABLE strategic_line');
        $this->addSql('DROP TABLE target_audience');
        $this->addSql('DROP TABLE task');
        $this->addSql('DROP TABLE team');
        $this->addSql('DROP TABLE user');
    }
}
