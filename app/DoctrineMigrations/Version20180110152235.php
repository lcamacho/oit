<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180110152235 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE objetives DROP FOREIGN KEY FK_4B64255F4F018C96');
        $this->addSql('DROP INDEX IDX_4B64255F4F018C96 ON objetives');
        $this->addSql('ALTER TABLE objetives DROP `lines`');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE objetives ADD `lines` INT DEFAULT NULL');
        $this->addSql('ALTER TABLE objetives ADD CONSTRAINT FK_4B64255F4F018C96 FOREIGN KEY (`lines`) REFERENCES line (id)');
        $this->addSql('CREATE INDEX IDX_4B64255F4F018C96 ON objetives (`lines`)');
    }
}
