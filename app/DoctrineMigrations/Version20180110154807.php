<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180110154807 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE strategic_line ADD line INT DEFAULT NULL, DROP strategicLine');
        $this->addSql('ALTER TABLE strategic_line ADD CONSTRAINT FK_8DF6A041D114B4F6 FOREIGN KEY (line) REFERENCES line (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_8DF6A041D114B4F6 ON strategic_line (line)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE strategic_line DROP FOREIGN KEY FK_8DF6A041D114B4F6');
        $this->addSql('DROP INDEX UNIQ_8DF6A041D114B4F6 ON strategic_line');
        $this->addSql('ALTER TABLE strategic_line ADD strategicLine INT NOT NULL, DROP line');
    }
}
