<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180110163947 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE strategic_line CHANGE Action action INT DEFAULT NULL, CHANGE endDate endDate DATE NOT NULL');
        $this->addSql('ALTER TABLE strategic_line ADD CONSTRAINT FK_8DF6A04147CC8C92 FOREIGN KEY (action) REFERENCES action (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_8DF6A04147CC8C92 ON strategic_line (action)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE strategic_line DROP FOREIGN KEY FK_8DF6A04147CC8C92');
        $this->addSql('DROP INDEX UNIQ_8DF6A04147CC8C92 ON strategic_line');
        $this->addSql('ALTER TABLE strategic_line CHANGE action Action INT NOT NULL, CHANGE endDate endDate VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci');
    }
}
