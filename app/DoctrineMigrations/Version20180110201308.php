<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180110201308 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE strategic_line DROP INDEX UNIQ_8DF6A041D114B4F6, ADD INDEX IDX_8DF6A041D114B4F6 (line)');
        $this->addSql('ALTER TABLE strategic_line DROP INDEX UNIQ_8DF6A04147CC8C92, ADD INDEX IDX_8DF6A04147CC8C92 (action)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE strategic_line DROP INDEX IDX_8DF6A041D114B4F6, ADD UNIQUE INDEX UNIQ_8DF6A041D114B4F6 (line)');
        $this->addSql('ALTER TABLE strategic_line DROP INDEX IDX_8DF6A04147CC8C92, ADD UNIQUE INDEX UNIQ_8DF6A04147CC8C92 (action)');
    }
}
