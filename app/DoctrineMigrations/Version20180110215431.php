<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180110215431 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE implementation_guide CHANGE author author INT DEFAULT NULL');
        $this->addSql('ALTER TABLE implementation_guide ADD CONSTRAINT FK_304E996FBDAFD8C8 FOREIGN KEY (author) REFERENCES user (id)');
        $this->addSql('CREATE INDEX IDX_304E996FBDAFD8C8 ON implementation_guide (author)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE implementation_guide DROP FOREIGN KEY FK_304E996FBDAFD8C8');
        $this->addSql('DROP INDEX IDX_304E996FBDAFD8C8 ON implementation_guide');
        $this->addSql('ALTER TABLE implementation_guide CHANGE author author INT NOT NULL');
    }
}
