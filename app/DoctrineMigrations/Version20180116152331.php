<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180116152331 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE team ADD objetives INT DEFAULT NULL, ADD principles INT DEFAULT NULL, ADD tasks INT DEFAULT NULL');
        $this->addSql('ALTER TABLE team ADD CONSTRAINT FK_C4E0A61F4B64255F FOREIGN KEY (objetives) REFERENCES objetives (id)');
        $this->addSql('ALTER TABLE team ADD CONSTRAINT FK_C4E0A61F3E8DCFE1 FOREIGN KEY (principles) REFERENCES principles (id)');
        $this->addSql('ALTER TABLE team ADD CONSTRAINT FK_C4E0A61F50586597 FOREIGN KEY (tasks) REFERENCES task (id)');
        $this->addSql('CREATE INDEX IDX_C4E0A61F4B64255F ON team (objetives)');
        $this->addSql('CREATE INDEX IDX_C4E0A61F3E8DCFE1 ON team (principles)');
        $this->addSql('CREATE INDEX IDX_C4E0A61F50586597 ON team (tasks)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE team DROP FOREIGN KEY FK_C4E0A61F4B64255F');
        $this->addSql('ALTER TABLE team DROP FOREIGN KEY FK_C4E0A61F3E8DCFE1');
        $this->addSql('ALTER TABLE team DROP FOREIGN KEY FK_C4E0A61F50586597');
        $this->addSql('DROP INDEX IDX_C4E0A61F4B64255F ON team');
        $this->addSql('DROP INDEX IDX_C4E0A61F3E8DCFE1 ON team');
        $this->addSql('DROP INDEX IDX_C4E0A61F50586597 ON team');
        $this->addSql('ALTER TABLE team DROP objetives, DROP principles, DROP tasks');
    }
}
