<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180116175410 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE guide_objetives (guide_id INT NOT NULL, objetive_id INT NOT NULL, INDEX IDX_6E3C5B9D7ED1D4B (guide_id), UNIQUE INDEX UNIQ_6E3C5B9F849066C (objetive_id), PRIMARY KEY(guide_id, objetive_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE guide_objetives ADD CONSTRAINT FK_6E3C5B9D7ED1D4B FOREIGN KEY (guide_id) REFERENCES team (id)');
        $this->addSql('ALTER TABLE guide_objetives ADD CONSTRAINT FK_6E3C5B9F849066C FOREIGN KEY (objetive_id) REFERENCES objetives (id)');
        $this->addSql('ALTER TABLE team DROP FOREIGN KEY FK_C4E0A61F4B64255F');
        $this->addSql('DROP INDEX IDX_C4E0A61F4B64255F ON team');
        $this->addSql('ALTER TABLE team DROP objetives');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE guide_objetives');
        $this->addSql('ALTER TABLE team ADD objetives INT DEFAULT NULL');
        $this->addSql('ALTER TABLE team ADD CONSTRAINT FK_C4E0A61F4B64255F FOREIGN KEY (objetives) REFERENCES objetives (id)');
        $this->addSql('CREATE INDEX IDX_C4E0A61F4B64255F ON team (objetives)');
    }
}
