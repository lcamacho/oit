<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180116175938 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE guide_principles (guide_id INT NOT NULL, principle_id INT NOT NULL, INDEX IDX_77A90F4CD7ED1D4B (guide_id), UNIQUE INDEX UNIQ_77A90F4CC2D0FBD5 (principle_id), PRIMARY KEY(guide_id, principle_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE guide_tasks (guide_id INT NOT NULL, task_id INT NOT NULL, INDEX IDX_1445AEDFD7ED1D4B (guide_id), UNIQUE INDEX UNIQ_1445AEDF8DB60186 (task_id), PRIMARY KEY(guide_id, task_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE guide_principles ADD CONSTRAINT FK_77A90F4CD7ED1D4B FOREIGN KEY (guide_id) REFERENCES team (id)');
        $this->addSql('ALTER TABLE guide_principles ADD CONSTRAINT FK_77A90F4CC2D0FBD5 FOREIGN KEY (principle_id) REFERENCES principles (id)');
        $this->addSql('ALTER TABLE guide_tasks ADD CONSTRAINT FK_1445AEDFD7ED1D4B FOREIGN KEY (guide_id) REFERENCES team (id)');
        $this->addSql('ALTER TABLE guide_tasks ADD CONSTRAINT FK_1445AEDF8DB60186 FOREIGN KEY (task_id) REFERENCES task (id)');
        $this->addSql('ALTER TABLE team DROP FOREIGN KEY FK_C4E0A61F3E8DCFE1');
        $this->addSql('ALTER TABLE team DROP FOREIGN KEY FK_C4E0A61F50586597');
        $this->addSql('DROP INDEX IDX_C4E0A61F3E8DCFE1 ON team');
        $this->addSql('DROP INDEX IDX_C4E0A61F50586597 ON team');
        $this->addSql('ALTER TABLE team DROP principles, DROP tasks');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE guide_principles');
        $this->addSql('DROP TABLE guide_tasks');
        $this->addSql('ALTER TABLE team ADD principles INT DEFAULT NULL, ADD tasks INT DEFAULT NULL');
        $this->addSql('ALTER TABLE team ADD CONSTRAINT FK_C4E0A61F3E8DCFE1 FOREIGN KEY (principles) REFERENCES principles (id)');
        $this->addSql('ALTER TABLE team ADD CONSTRAINT FK_C4E0A61F50586597 FOREIGN KEY (tasks) REFERENCES task (id)');
        $this->addSql('CREATE INDEX IDX_C4E0A61F3E8DCFE1 ON team (principles)');
        $this->addSql('CREATE INDEX IDX_C4E0A61F50586597 ON team (tasks)');
    }
}
