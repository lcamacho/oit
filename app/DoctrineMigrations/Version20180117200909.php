<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180117200909 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE improvement_by_guide (id INT AUTO_INCREMENT NOT NULL, guide_id INT DEFAULT NULL, improvement_id INT DEFAULT NULL, answer INT NOT NULL, INDEX IDX_D5C64C4BD7ED1D4B (guide_id), INDEX IDX_D5C64C4BEAB2C33C (improvement_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE improvement_point (id INT AUTO_INCREMENT NOT NULL, description VARCHAR(200) NOT NULL, improvement VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE improvement_by_guide ADD CONSTRAINT FK_D5C64C4BD7ED1D4B FOREIGN KEY (guide_id) REFERENCES implementation_guide (id)');
        $this->addSql('ALTER TABLE improvement_by_guide ADD CONSTRAINT FK_D5C64C4BEAB2C33C FOREIGN KEY (improvement_id) REFERENCES improvement_point (id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE improvement_by_guide DROP FOREIGN KEY FK_D5C64C4BEAB2C33C');
        $this->addSql('DROP TABLE improvement_by_guide');
        $this->addSql('DROP TABLE improvement_point');
    }
}
