<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180118150234 extends AbstractMigration {

    /**
     * @param Schema $schema
     */
    public function up(Schema $schema) {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE improvement_point CHANGE description description VARCHAR(255) NOT NULL');
        $this->addSql("INSERT INTO `improvement_point` (`id`, `description`, `improvement`) VALUES (NULL, 'Nuestra empresa cuenta con un Reglamento que norma el funcionamiento del Equipo/ Comité de VIH.', 'Crear con un Reglamento que norma el funcionamiento del Equipo/ Comité de VIH.');");
        $this->addSql("INSERT INTO `improvement_point` (`id`, `description`, `improvement`) VALUES (NULL, 'Nuestra empresa dispone al servicio de los programas de VIH una estructura y recursos ya existentes que pueden ser utilizados para la implementación del programa de VIH.', 'Disponer al servicio de los programas de VIH una estructura y recursos ya existentes que pueden ser utilizados para la implementación del programa de VIH.');");
        $this->addSql("INSERT INTO `improvement_point` (`id`, `description`, `improvement`) VALUES (NULL, 'Nuestra empresa prohibe la obligatoriedad de las pruebas de detección del VIH antes o durante el empleo.', 'Prohibir la obligatoriedad de las pruebas de detección del VIH antes o durante el empleo.');");
        $this->addSql("INSERT INTO `improvement_point` (`id`, `description`, `improvement`) VALUES (NULL, 'Nuestra empresa no despide ni discrimina a personas con VIH o afectadas por el VIH.', 'No despedir ni discriminar a personas con VIH o afectadas por el VIH.');");
        $this->addSql("INSERT INTO `improvement_point` (`id`, `description`, `improvement`) VALUES (NULL, 'Nuestra empresa conoce la incidencia del VIH en la comunidad donde se instala nuestro ingenio u organización asociada, así como los servicios ya disponibles en el lugar de trabajo o en sus proximidades.', 'Conocer la incidencia del VIH en la comunidad donde se instala nuestro ingenio u organización asociada, así como los servicios ya disponibles en el lugar de trabajo o en sus proximidades.');");
        $this->addSql("INSERT INTO `improvement_point` (`id`, `description`, `improvement`) VALUES (NULL, 'Nuestra empresa cuenta con un protocolo de privacidad y confidencialidad de datos de las personas con VIH.', 'Contar con un protocolo de privacidad y confidencialidad de datos de las personas con VIH.');");
        $this->addSql("INSERT INTO `improvement_point` (`id`, `description`, `improvement`) VALUES (NULL, 'Nuestra empresa cuenta con un Protocolo de Pre y Post Consejería para la realización de pruebas de VIH.', 'Contar con un Protocolo de Pre y Post Consejería para la realización de pruebas de VIH.');");
        $this->addSql("INSERT INTO `improvement_point` (`id`, `description`, `improvement`) VALUES (NULL, 'Nuestra empresa cuenta con una ruta establecida de asistencia y apoyo para las personas con VIH.', 'Crear una ruta establecida de asistencia y apoyo para las personas con VIH.');");
        $this->addSql("INSERT INTO `improvement_point` (`id`, `description`, `improvement`) VALUES (NULL, 'Nuestra empresa cuenta con un presupuesto para VIH.', 'Asignar un presupuesto para VIH.');");
        $this->addSql("INSERT INTO `improvement_point` (`id`, `description`, `improvement`) VALUES (NULL, 'Nuestra empresa cuenta con una identificación de públicos meta para dirigir acciones específicas del programa de VIH.', 'Identificar públicos meta para dirigir acciones específicas del programa de VIH.');");
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema) {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE improvement_point CHANGE description description VARCHAR(200) NOT NULL COLLATE utf8_unicode_ci');
    }

}
