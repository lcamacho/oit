<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180118154848 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE target_audience ADD guide_id INT DEFAULT NULL, CHANGE sector sector VARCHAR(150) DEFAULT NULL, CHANGE numberOfMens numberOfMens INT DEFAULT NULL, CHANGE numberOfWomens numberOfWomens INT DEFAULT NULL, CHANGE minAgeRange minAgeRange INT DEFAULT NULL, CHANGE maxAgeRange maxAgeRange INT DEFAULT NULL, CHANGE profile profile VARCHAR(255) DEFAULT NULL, CHANGE VIHRiskLevel VIHRiskLevel INT DEFAULT NULL');
        $this->addSql('ALTER TABLE target_audience ADD CONSTRAINT FK_27664471D7ED1D4B FOREIGN KEY (guide_id) REFERENCES implementation_guide (id)');
        $this->addSql('CREATE INDEX IDX_27664471D7ED1D4B ON target_audience (guide_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE target_audience DROP FOREIGN KEY FK_27664471D7ED1D4B');
        $this->addSql('DROP INDEX IDX_27664471D7ED1D4B ON target_audience');
        $this->addSql('ALTER TABLE target_audience DROP guide_id, CHANGE sector sector VARCHAR(150) NOT NULL COLLATE utf8_unicode_ci, CHANGE numberOfMens numberOfMens INT NOT NULL, CHANGE numberOfWomens numberOfWomens INT NOT NULL, CHANGE minAgeRange minAgeRange INT NOT NULL, CHANGE maxAgeRange maxAgeRange INT NOT NULL, CHANGE profile profile VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, CHANGE VIHRiskLevel VIHRiskLevel INT NOT NULL');
    }
}
