<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180118181207 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE guide_objetives DROP INDEX UNIQ_6E3C5B9F849066C, ADD INDEX IDX_6E3C5B9F849066C (objetive_id)');
        $this->addSql('ALTER TABLE guide_principles DROP INDEX UNIQ_77A90F4CC2D0FBD5, ADD INDEX IDX_77A90F4CC2D0FBD5 (principle_id)');
        $this->addSql('ALTER TABLE guide_tasks DROP INDEX UNIQ_1445AEDF8DB60186, ADD INDEX IDX_1445AEDF8DB60186 (task_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE guide_objetives DROP INDEX IDX_6E3C5B9F849066C, ADD UNIQUE INDEX UNIQ_6E3C5B9F849066C (objetive_id)');
        $this->addSql('ALTER TABLE guide_principles DROP INDEX IDX_77A90F4CC2D0FBD5, ADD UNIQUE INDEX UNIQ_77A90F4CC2D0FBD5 (principle_id)');
        $this->addSql('ALTER TABLE guide_tasks DROP INDEX IDX_1445AEDF8DB60186, ADD UNIQUE INDEX UNIQ_1445AEDF8DB60186 (task_id)');
    }
}
