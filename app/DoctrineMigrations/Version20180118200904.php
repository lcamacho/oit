<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180118200904 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE allies ADD guide_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE allies ADD CONSTRAINT FK_C9D8E060D7ED1D4B FOREIGN KEY (guide_id) REFERENCES implementation_guide (id)');
        $this->addSql('CREATE INDEX IDX_C9D8E060D7ED1D4B ON allies (guide_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE allies DROP FOREIGN KEY FK_C9D8E060D7ED1D4B');
        $this->addSql('DROP INDEX IDX_C9D8E060D7ED1D4B ON allies');
        $this->addSql('ALTER TABLE allies DROP guide_id');
    }
}
