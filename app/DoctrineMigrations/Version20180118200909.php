<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180118200909 extends AbstractMigration {

    /**
     * @param Schema $schema
     */
    public function up(Schema $schema) {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql("INSERT INTO `objetives` (`id`, `title`, `image`) VALUES (NULL, 'Crear los mecanismos para el respeto de los derechos de los colaboradores con VIH o VIH avanzado.', '');");
        $this->addSql("INSERT INTO `objetives` (`id`, `title`, `image`) VALUES (NULL, 'Desarrollar programas de educación y prevención de VIH con la población laboral y sus familias.', '');");
        $this->addSql("INSERT INTO `objetives` (`id`, `title`, `image`) VALUES (NULL, 'Establecer la metodología de orientación y apoyo a los colaboradores con VIH.', '');");
        $this->addSql("INSERT INTO `objetives` (`id`, `title`, `image`) VALUES (NULL, 'Firma de acuerdos y/o alianzas con otros actores locales de salud, públicos y privados para dar una respuesta integrada a las necesidades que plantea el VIH.', '');");
        $this->addSql("INSERT INTO `objetives` (`id`, `title`, `image`) VALUES (NULL, 'Fomentar y promover que cada individuo asuma su responsabilidad y compromiso en la prevención, diagnóstico y cuidado con el VIH para consigo y su familia.', '');");

        $this->addSql("INSERT INTO `principles` (`id`, `description`, `icon`) VALUES (NULL, 'Corresponsabilidad de los colaboradores en las acciones de prevención, atención y cuidado del VIH.', '');");
        $this->addSql("INSERT INTO `principles` (`id`, `description`, `icon`) VALUES (NULL, 'No discriminar a ningún colaborador con VIH.', '');");
        $this->addSql("INSERT INTO `principles` (`id`, `description`, `icon`) VALUES (NULL, 'No exigir un resultado de laboratorio y/o no realizar la prueba de VIH como requisito para acceder o mantener un empleo.', '');");
        $this->addSql("INSERT INTO `principles` (`id`, `description`, `icon`) VALUES (NULL, 'Derecho a la confidencialidad.', '');");
        $this->addSql("INSERT INTO `principles` (`id`, `description`, `icon`) VALUES (NULL, 'Promoción y establecimiento de alianzas y acuerdos.', '');");
        $this->addSql("INSERT INTO `principles` (`id`, `description`, `icon`) VALUES (NULL, 'Reconocer y aceptar que el VIH y el VIH avanzado es un problema de salud que debe ser tratado como una enfermedad/condición seria.', '');");
        $this->addSql("INSERT INTO `principles` (`id`, `description`, `icon`) VALUES (NULL, 'Promover y reconocer la igualdad de oportunidades entre hombres y mujeres en la aplicación de esta política.', '');");
        $this->addSql("INSERT INTO `principles` (`id`, `description`, `icon`) VALUES (NULL, 'Asegurar un ambiente de trabajo saludable.', '');");
        $this->addSql("INSERT INTO `principles` (`id`, `description`, `icon`) VALUES (NULL, 'Desarrollar y facilitar acciones periódicas de información, educación y sensibilización acerca del VIH.', '');");

        $this->addSql("INSERT INTO `task` (`id`, `description`, `comment`) VALUES (NULL, 'Coordinador el Equipo/Comité de VIH', '');");
        $this->addSql("INSERT INTO `task` (`id`, `description`, `comment`) VALUES (NULL, 'Redactar el Reglamento del Equipo/Comité de VIH', '');");
        $this->addSql("INSERT INTO `task` (`id`, `description`, `comment`) VALUES (NULL, 'Difundir la Política de VIH en el Lugar de Trabajo', '');");
        $this->addSql("INSERT INTO `task` (`id`, `description`, `comment`) VALUES (NULL, 'Mediar entre todas las áreas de la empresa para facilitar la implementación de acciones', '');");
        $this->addSql("INSERT INTO `task` (`id`, `description`, `comment`) VALUES (NULL, 'Solicitar contenidos a las áreas correspondientes para integrar el VIH a las inducciones de todos los niveles', '');");
        $this->addSql("INSERT INTO `task` (`id`, `description`, `comment`) VALUES (NULL, 'Representar al Comité ante el resto de Comités de VIH de ASAZGUA', '');");
        $this->addSql("INSERT INTO `task` (`id`, `description`, `comment`) VALUES (NULL, 'Elaborar el programa de formación de educadores', '');");
        $this->addSql("INSERT INTO `task` (`id`, `description`, `comment`) VALUES (NULL, 'Elaborar programas de información y sensibilización', '');");
        $this->addSql("INSERT INTO `task` (`id`, `description`, `comment`) VALUES (NULL, 'Elaborar programas de educación participativa.', '');");
        $this->addSql("INSERT INTO `task` (`id`, `description`, `comment`) VALUES (NULL, 'Seleccionar al equipo de formadores y educadores pares.', '');");
        $this->addSql("INSERT INTO `task` (`id`, `description`, `comment`) VALUES (NULL, 'Garantizar la no realización de pruebas de VIH obligatorias o presentación de resultados', '');");
        $this->addSql("INSERT INTO `task` (`id`, `description`, `comment`) VALUES (NULL, 'Garantizar la no discriminación en la contratación por cuestión de estado serológico', '');");
        $this->addSql("INSERT INTO `task` (`id`, `description`, `comment`) VALUES (NULL, 'Elaborar los informes de monitoreo y evaluación internos, a partir de los reportes de los colegas.', '');");
        $this->addSql("INSERT INTO `task` (`id`, `description`, `comment`) VALUES (NULL, 'Desarrollar acciones relacionadas con prevención y atención de VIH en el Programa Externo de RSE.', '');");
        $this->addSql("INSERT INTO `task` (`id`, `description`, `comment`) VALUES (NULL, 'Coordinar con las Escuelas de niños y adultos de los ingenios la integración de contenidos relacionados con VIH.', '');");
        $this->addSql("INSERT INTO `task` (`id`, `description`, `comment`) VALUES (NULL, 'Representar al Comité de VIH ante la Red Multisectorial departamental', '');");
        $this->addSql("INSERT INTO `task` (`id`, `description`, `comment`) VALUES (NULL, 'Elaborar el protocolo de Precauciones Universales de Seguridad ante el VIH', '');");
        $this->addSql("INSERT INTO `task` (`id`, `description`, `comment`) VALUES (NULL, 'Elaborar la Estrategia de Distribución de Condones en la empresa.', '');");
        $this->addSql("INSERT INTO `task` (`id`, `description`, `comment`) VALUES (NULL, 'Elaborar el Protocolo de Privacidad y Confidencialidad para los casos identificados de VIH', '');");
        $this->addSql("INSERT INTO `task` (`id`, `description`, `comment`) VALUES (NULL, 'Elaborar el Protocolo de Profilaxis Post Exposición', '');");
        $this->addSql("INSERT INTO `task` (`id`, `description`, `comment`) VALUES (NULL, 'Elaborar el Protocolo de Pre y post Consejería y Prueba de VIH', '');");
        $this->addSql("INSERT INTO `task` (`id`, `description`, `comment`) VALUES (NULL, 'Ser enlace con las entidades locales, departamentales y nacionales de servicios de salud.', '');");
        $this->addSql("INSERT INTO `task` (`id`, `description`, `comment`) VALUES (NULL, 'Coordinar con los servicios de salud gubernamentales y no gubernamentales la realización de campañas de pruebas voluntarias de VIH', '');");
        $this->addSql("INSERT INTO `task` (`id`, `description`, `comment`) VALUES (NULL, 'Coordinar acciones de información y sensibilización con actores gubernamentales y no gubernamentales en fechas clave para el VIH (Candlelight, Día de la Diversidad Sexual, Día Mundial del Sida)', '');");
        $this->addSql("INSERT INTO `task` (`id`, `description`, `comment`) VALUES (NULL, 'Coordinar la realización de jornadas médicas, con abordaje de VIH, en los municipios y departamentos de donde proceden los trabajadores de la empresa.', '');");
        $this->addSql("INSERT INTO `task` (`id`, `description`, `comment`) VALUES (NULL, 'Elaborar el Protocolo de Seguimiento y Atención a mujeres embarazadas.', '');");
        $this->addSql("INSERT INTO `task` (`id`, `description`, `comment`) VALUES (NULL, 'Apoyar y orientar a las personas con VIH y a sus familias.', '');");
        $this->addSql("INSERT INTO `task` (`id`, `description`, `comment`) VALUES (NULL, 'Elaborar materiales de información y sensibilización impresos, digitales y audiovisuales.', '');");
        $this->addSql("INSERT INTO `task` (`id`, `description`, `comment`) VALUES (NULL, 'Promover la no discriminación', '');");
        $this->addSql("INSERT INTO `task` (`id`, `description`, `comment`) VALUES (NULL, 'Promover la igualdad de género', '');");
        $this->addSql("INSERT INTO `task` (`id`, `description`, `comment`) VALUES (NULL, 'Abordar el diagnóstico y tratamiento de ITS.', '');");
        $this->addSql("INSERT INTO `task` (`id`, `description`, `comment`) VALUES (NULL, 'Promover las medidas de cambio de comportamiento', '');");
        $this->addSql("INSERT INTO `task` (`id`, `description`, `comment`) VALUES (NULL, 'Promover la promoción de una atmósfera de confianza', '');");
        $this->addSql("INSERT INTO `task` (`id`, `description`, `comment`) VALUES (NULL, 'Diseñar e implementar campañas de comunicación.', '');");
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema) {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');
    }

}
