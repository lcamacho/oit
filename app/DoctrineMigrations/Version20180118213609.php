<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180118213609 extends AbstractMigration {

    /**
     * @param Schema $schema
     */
    public function up(Schema $schema) {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE line CHANGE description description VARCHAR(260) NOT NULL');
        $this->addSql("UPDATE `principles` SET `icon` = '/oit/web/bundles/oit/images/img-principios-item1.svg' WHERE `principles`.`id` = 1;");
        $this->addSql("UPDATE `principles` SET `icon` = '/oit/web/bundles/oit/images/img-principios-item2.svg' WHERE `principles`.`id` = 2;");
        $this->addSql("UPDATE `principles` SET `icon` = '/oit/web/bundles/oit/images/img-principios-item3.svg' WHERE `principles`.`id` = 3;");
        $this->addSql("UPDATE `principles` SET `icon` = '/oit/web/bundles/oit/images/img-principios-item4.svg' WHERE `principles`.`id` = 4;");
        $this->addSql("UPDATE `principles` SET `icon` = '/oit/web/bundles/oit/images/img-principios-item5.svg' WHERE `principles`.`id` = 5;");
        $this->addSql("UPDATE `principles` SET `icon` = '/oit/web/bundles/oit/images/img-principios-item6.svg' WHERE `principles`.`id` = 6;");
        $this->addSql("UPDATE `principles` SET `icon` = '/oit/web/bundles/oit/images/img-principios-item7.svg' WHERE `principles`.`id` = 7;");
        $this->addSql("UPDATE `principles` SET `icon` = '/oit/web/bundles/oit/images/img-principios-item8.svg' WHERE `principles`.`id` = 8;");
        $this->addSql("UPDATE `principles` SET `icon` = '/oit/web/bundles/oit/images/img-principios-item9.svg' WHERE `principles`.`id` = 9;");

        $this->addSql("UPDATE `objetives` SET `image` = '/oit/web/bundles/oit/images/img-objetivos-item1.svg' WHERE `objetives`.`id` = 1;");
        $this->addSql("UPDATE `objetives` SET `image` = '/oit/web/bundles/oit/images/img-objetivos-item2.svg' WHERE `objetives`.`id` = 2;");
        $this->addSql("UPDATE `objetives` SET `image` = '/oit/web/bundles/oit/images/img-objetivos-item3.svg' WHERE `objetives`.`id` = 3;");
        $this->addSql("UPDATE `objetives` SET `image` = '/oit/web/bundles/oit/images/img-objetivos-item4.svg' WHERE `objetives`.`id` = 4;");
        $this->addSql("UPDATE `objetives` SET `image` = '/oit/web/bundles/oit/images/img-objetivos-item5.svg' WHERE `objetives`.`id` = 5;");

        $this->addSql("INSERT INTO `line` (`id`, `description`, `objetive_id`) VALUES ('1', 'Información y sensibilización', NULL);");
        $this->addSql("INSERT INTO `line` (`id`, `description`, `objetive_id`) VALUES ('2', 'Formación de educadores', NULL);");
        $this->addSql("INSERT INTO `line` (`id`, `description`, `objetive_id`) VALUES ('3', 'Programas de educación participativa', NULL);");
        $this->addSql("INSERT INTO `line` (`id`, `description`, `objetive_id`) VALUES ('4', 'Medidas prácticas que respalden el cambio de comportamiento', NULL);");
        $this->addSql("INSERT INTO `line` (`id`, `description`, `objetive_id`) VALUES ('5', 'Pruebas de VIH', NULL);");
        $this->addSql("INSERT INTO `line` (`id`, `description`, `objetive_id`) VALUES ('6', 'Mujeres embarazadas', NULL);");
        $this->addSql("INSERT INTO `line` (`id`, `description`, `objetive_id`) VALUES ('7', 'Servicios de asistencia', NULL);");
        $this->addSql("INSERT INTO `line` (`id`, `description`, `objetive_id`) VALUES ('8', 'Servicios de apoyo', NULL);");
        $this->addSql("INSERT INTO `line` (`id`, `description`, `objetive_id`) VALUES ('9', 'Servicios de tratamiento', NULL);");
        $this->addSql("INSERT INTO `line` (`id`, `description`, `objetive_id`) VALUES ('10', 'Transmisión madre-hijo', NULL);");
        $this->addSql("INSERT INTO `line` (`id`, `description`, `objetive_id`) VALUES ('11', 'Cero discriminación', NULL);");
        $this->addSql("INSERT INTO `line` (`id`, `description`, `objetive_id`) VALUES ('12', 'Igualdad de género', NULL);");
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema) {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE line CHANGE description description VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci');
    }

}
