<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180118230304 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE resource_category (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE resource ADD resourcecategory_id INT DEFAULT NULL, DROP category');
        $this->addSql('ALTER TABLE resource ADD CONSTRAINT FK_BC91F41611152A1C FOREIGN KEY (resourcecategory_id) REFERENCES resource_category (id)');
        $this->addSql('CREATE INDEX IDX_BC91F41611152A1C ON resource (resourcecategory_id)');
        
        $this->addSql("INSERT INTO `resource_category` (`id`, `name`) VALUES ('1', 'Marco normativo y legal');");
        $this->addSql("INSERT INTO `resource_category` (`id`, `name`) VALUES ('2', 'Manuales');");
        $this->addSql("INSERT INTO `resource_category` (`id`, `name`) VALUES ('3', 'Apoyo para la elaboración de materiales y programas');");
        $this->addSql("INSERT INTO `resource_category` (`id`, `name`) VALUES ('4', 'Campañas sobre VIH');");
        $this->addSql("INSERT INTO `resource_category` (`id`, `name`) VALUES ('5', 'Buenas prácticas');");
        $this->addSql("INSERT INTO `resource_category` (`id`, `name`) VALUES ('6', 'Webs de referencia');");
        $this->addSql("INSERT INTO `resource_category` (`id`, `name`) VALUES ('7', 'Directorio de VIH de Guatemala');");
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE resource DROP FOREIGN KEY FK_BC91F41611152A1C');
        $this->addSql('DROP TABLE resource_category');
        $this->addSql('DROP INDEX IDX_BC91F41611152A1C ON resource');
        $this->addSql('ALTER TABLE resource ADD category INT NOT NULL, DROP resourcecategory_id');
    }
}
