<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180118231509 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE resource CHANGE title title VARCHAR(300) NOT NULL');
        
                $this->addSql("INSERT INTO `resource_category` (`id`, `name`) VALUES ('1', 'Marco normativo y legal');");
        $this->addSql("INSERT INTO `resource_category` (`id`, `name`) VALUES ('2', 'Manuales');");
        $this->addSql("INSERT INTO `resource_category` (`id`, `name`) VALUES ('3', 'Apoyo para la elaboración de materiales y programas');");
        $this->addSql("INSERT INTO `resource_category` (`id`, `name`) VALUES ('4', 'Campañas sobre VIH');");
        $this->addSql("INSERT INTO `resource_category` (`id`, `name`) VALUES ('5', 'Buenas prácticas');");
        $this->addSql("INSERT INTO `resource_category` (`id`, `name`) VALUES ('6', 'Webs de referencia');");
        $this->addSql("INSERT INTO `resource_category` (`id`, `name`) VALUES ('7', 'Directorio de VIH de Guatemala');");
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE resource CHANGE title title VARCHAR(200) NOT NULL COLLATE utf8_unicode_ci');
    }
}
