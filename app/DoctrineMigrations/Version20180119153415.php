<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180119153415 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql("INSERT INTO `resource` (`id`, `title`, `link`, `resourcecategory_id`) VALUES (NULL, 'Ley SIDA Guatemala - Ley General para el combate del Virus de Inmunodeficiencia Humana -VIH- y del Síndrome de Inmunodeficiencia Adquirida - SIDA- y de la promoción, protección y defensa de los derechos humanos ante el VIH/SIDA. 26 de junio de 2000', 'http://www.pasca.org/sites/default/files/ley_sida_gt.pdf', '1');");

$this->addSql("INSERT INTO `resource` (`id`, `title`, `link`, `resourcecategory_id`) VALUES (NULL, 'Reglamento Ley SIDA Guatemala - Reglamento de la Ley General para el combate del Virus de Inmunodeficiencia Humana -VIH- y del Síndrome de Inmunodeficiencia Adquirida - SIDA- y de la promoción, protección y defensa de los derechos humanos ante el VIH/SIDA. 6 de septiembre de 2002
', 'http://www.pasca.org/userfiles/REGLAMENTO_LEY_SIDA_AG317_2002.pdf', '1');");

$this->addSql("INSERT INTO `resource` (`id`, `title`, `link`, `resourcecategory_id`) VALUES (NULL, 'Política Publica 638-2005 Respecto de la prevención a las Infecciones de Transmisión Sexual – ITS - y a la respuesta a la epidemia del Síndrome de Inmunodeficiencia Adquirida – sida', 'http://www.pasca.org/sites/default/files/wcms_132631.pdf', '1');");

$this->addSql("INSERT INTO `resource` (`id`, `title`, `link`, `resourcecategory_id`) VALUES (NULL, 'Decreto 90-97 Código de Salud', 'http://www.mspas.gob.gt/files/Descargas/DGRVCS/Salud_publica/Decretos/DEC_50_2000.pdf', '1');");

$this->addSql("INSERT INTO `resource` (`id`, `title`, `link`, `resourcecategory_id`) VALUES (NULL, 'Reglamento de Salud y Seguridad Ocupacional', 'http://www.mintrabajo.gob.gt/images/organizacion/leyesconveniosyacuerdos/Leyes_Ordinarias/ACUERDO_GUBERNATIVO_229-2014.pdf
', '1');");

$this->addSql("INSERT INTO `resource` (`id`, `title`, `link`, `resourcecategory_id`) VALUES (NULL, 'Reformas al Reglamento de Salud y Seguridad Ocupacional - Acuerdo Gubernativo 51-2015', 'http://www.mintrabajo.gob.gt/images/organizacion/leyesconveniosyacuerdos/Leyes_Ordinarias/Acdo%20Gub%2051-2015%20Reforma%20Reglamento%20Salud%20y%20Seguridad.pdf', '1');");

$this->addSql("INSERT INTO `resource` (`id`, `title`, `link`, `resourcecategory_id`) VALUES (NULL, 'Reforma al acuerdo Gubernativo 229-2014 de fecha 23 de julio de 2014, Reglamento de Salud y Seguridad Ocupacional', 'http://www.mintrabajo.gob.gt/images/organizacion/leyesconveniosyacuerdos/Leyes_Ordinarias/Acdo_Gub_199-2015_Reforma_Reglamento_Salud_y_Seguridad.pdf', '1');");

$this->addSql("INSERT INTO `resource` (`id`, `title`, `link`, `resourcecategory_id`) VALUES (NULL, 'Primera Norma Internacional del trabajo sobre el VIH y el sida: Prevenir el VIH, Proteger los derechos humanos en el trabajo (folleto en Español) ', 'http://www.ilo.org/wcmsp5/groups/public/---ed_protect/---protrav/---ilo_aids/documents/publication/wcms_183942.pdf 
', '1');");

$this->addSql("INSERT INTO `resource` (`id`, `title`, `link`, `resourcecategory_id`) VALUES (NULL, 'Recomendación sobre el VIH y el sida y el mundo del trabajo, 2010 (núm.200)', 'http://www.ilo.org/aids/WCMS_142708/lang--es/index.htm', '1');");

$this->addSql("INSERT INTO `resource` (`id`, `title`, `link`, `resourcecategory_id`) VALUES (NULL, 'Aplicación de las recomendaciones prácticas de la OIT sobre el VIH / Sida y el Mundo del Trabajo. OIT 
', 'http://www.ilo.org/public/libdoc/ilo/2003/103B09_46_span.pdf', '2');");

$this->addSql("INSERT INTO `resource` (`id`, `title`, `link`, `resourcecategory_id`) VALUES (NULL, 'Programas sobre VIH/Sida en el lugar de trabajo. Una guía de acción para gerentes escrita por Bill Rau. USAID
', 'http://www.pasca.org/sites/default/files/ProgramasVIHlugartrabajoHVsp.pdf', '2');");

$this->addSql("INSERT INTO `resource` (`id`, `title`, `link`, `resourcecategory_id`) VALUES (NULL, 'Iniciativas sobre VIH en el Lugar de Trabajo. USAID', 'http://pdf.usaid.gov/pdf_docs/pnadp277.pdf', '2');");

$this->addSql("INSERT INTO `resource` (`id`, `title`, `link`, `resourcecategory_id`) VALUES (NULL, 'Información básica sobre el VIH. CDC', 'http://www.cdc.gov/hiv/spanish/basics/index.html', '3');");

$this->addSql("INSERT INTO `resource` (`id`, `title`, `link`, `resourcecategory_id`) VALUES (NULL, 'VIH e Infecciones sobre Transmisión Sexual. CDC', 'http://www.cdc.gov/std/spanish/vih/default.htm', '3');");

$this->addSql("INSERT INTO `resource` (`id`, `title`, `link`, `resourcecategory_id`) VALUES (NULL, 'Infografías ONUSIDA', 'http://www.unaids.org/es/resources/infographics', '3');");

$this->addSql("INSERT INTO `resource` (`id`, `title`, `link`, `resourcecategory_id`) VALUES (NULL, 'Fichas informativas ONUSIDA', 'http://www.unaids.org/es/resources/campaigns/HowAIDSchangedeverything/factsheet', '3');");

$this->addSql("INSERT INTO `resource` (`id`, `title`, `link`, `resourcecategory_id`) VALUES (NULL, 'Información sobre condones. UNFPA, OMS y ONUSIDA', 'http://www.unaids.org/es/resources/presscentre/featurestories/2015/july/20150702_condoms_prevention', '3');");

$this->addSql("INSERT INTO `resource` (`id`, `title`, `link`, `resourcecategory_id`) VALUES (NULL, 'Prevención del VIH a través de las nuevas tecnologías de la información y la comunicación. Ministerio de Salud, Política Social e Igualdad. España', 'http://www.msssi.gob.es/ciudadanos/enfLesiones/enfTransmisibles/sida/docs/VIH_TIC.pdf', '3');");

$this->addSql("INSERT INTO `resource` (`id`, `title`, `link`, `resourcecategory_id`) VALUES (NULL, 'Formación de educadores. UNESCO', 'http://www.unesco.org/new/es/hiv-and-aids/our-priorities-in-hiv/educaids/educator-training-and-support/', '3');");


$this->addSql("INSERT INTO `resource` (`id`, `title`, `link`, `resourcecategory_id`) VALUES (NULL, 'Recursos para educadores. Obra Social La Caixa. España', 'http://www.xplorehealth.eu/es/resources-for-educators/2780?keywords=VIH&module=All&categoria=All', '3');");

$this->addSql("INSERT INTO `resource` (`id`, `title`, `link`, `resourcecategory_id`) VALUES (NULL, 'Guía para el manejo y seguimiento de la mujer embarazada con VIH o con sida. Programa Nacional de Sida. Guatemala', 'http://www.mspas.gob.gt/files/Descargas/VIHSIDA/Programa%20Nacional%20de%20prevencion%20y%20control%20de%20ITS%20VIH%20y%20Sida.pdf', '3');");

$this->addSql("INSERT INTO `resource` (`id`, `title`, `link`, `resourcecategory_id`) VALUES (NULL, 'Guía del usuario de la herramienta de cálculo de costos relativos a los derechos humanos en el contexto del VIH', 'http://www.unaids.org/sites/default/files/en/media/unaids/contentassets/documents/document/2012/JC2276_HRCT_UserGuide_es.pdf', '3');");

$this->addSql("INSERT INTO `resource` (`id`, `title`, `link`, `resourcecategory_id`) VALUES (NULL, 'El Estigma Alimenta al VIH. Campaña Mundial para los Centros de Trabajo de Naciones Unidas', 'http://www.bestigmafree.org/es/index.html', '4');");

$this->addSql("INSERT INTO `resource` (`id`, `title`, `link`, `resourcecategory_id`) VALUES (NULL, 'Conjunto de Herramientas para Estrategias de Incidencia. ONUSIDA ', 'http://www.unaids.org/es/resources/documents/2014/advocacy_toolkit', '4');");

$this->addSql("INSERT INTO `resource` (`id`, `title`, `link`, `resourcecategory_id`) VALUES (NULL, 'Cero Discriminacón. ONUSIDA América Latina', 'http://cerodiscriminacion.onusida-latina.org', '4');");

$this->addSql("INSERT INTO `resource` (`id`, `title`, `link`, `resourcecategory_id`) VALUES (NULL, 'Behaviour Change Communication: An ILO/FHI toolkit for the workplace (OIT y FHI) (Inglés)', 'http://www.ilo.org/wcmsp5/groups/public/@ed_protect/@protrav/@ilo_aids/documents/publication/wcms_115460.pdf', '4');");

$this->addSql("INSERT INTO `resource` (`id`, `title`, `link`, `resourcecategory_id`) VALUES (NULL, 'Facilitar los estudios elaborados por ASAZGUA sobre VIH.', '#', '5');");

$this->addSql("INSERT INTO `resource` (`id`, `title`, `link`, `resourcecategory_id`) VALUES (NULL, 'OIT', 'http://www.ilo.org/global/about-the-ilo/lang--es/index.htm', '6');");

$this->addSql("INSERT INTO `resource` (`id`, `title`, `link`, `resourcecategory_id`) VALUES (NULL, 'ONUSIDA', 'http://www.unaids.org/es', '6');");

$this->addSql("INSERT INTO `resource` (`id`, `title`, `link`, `resourcecategory_id`) VALUES (NULL, 'ONUSIDA Latina', 'http://onusida-latina.org/es/', '6');");


$this->addSql("INSERT INTO `resource` (`id`, `title`, `link`, `resourcecategory_id`) VALUES (NULL, 'Pasca Guatemala. Programa de USAID para fortalecer la respuesta Centroamericana ante el VIH', 'http://www.pasca.org/content/guatemala', '6');");

$this->addSql("INSERT INTO `resource` (`id`, `title`, `link`, `resourcecategory_id`) VALUES (NULL, 'Centro Internacional de Formación de la OIT', 'http://www.itcilo.org/es/el-centro/programas/actividades-para-los-trabajadores/?set_language=es', '6');");

$this->addSql("INSERT INTO `resource` (`id`, `title`, `link`, `resourcecategory_id`) VALUES (NULL, 'Buenas experiencias. Compromiso Laboral VIH ', 'http://compromisolaboralvih.org', '6');");

$this->addSql("INSERT INTO `resource` (`id`, `title`, `link`, `resourcecategory_id`) VALUES (NULL, 'Centros para el Control y la Prevención de Enfermedades ', 'http://www.cdc.gov/hiv/spanish/default.html ', '6');");




        
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

    }
}
