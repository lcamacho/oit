<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180119154740 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE resource_category ADD icon VARCHAR(255) NOT NULL');
        
        $this->addSql("UPDATE `resource_category` SET `icon` = '/oit/web/bundles/oit/images/recursos-item1.svg' WHERE `resource_category`.`id` = 1;");
        $this->addSql("UPDATE `resource_category` SET `icon` = '/oit/web/bundles/oit/images/recursos-item2.svg' WHERE `resource_category`.`id` = 2;");
        $this->addSql("UPDATE `resource_category` SET `icon` = '/oit/web/bundles/oit/images/recursos-item3.svg' WHERE `resource_category`.`id` = 3;");
        $this->addSql("UPDATE `resource_category` SET `icon` = '/oit/web/bundles/oit/images/recursos-item4.svg' WHERE `resource_category`.`id` = 4;");
        $this->addSql("UPDATE `resource_category` SET `icon` = '/oit/web/bundles/oit/images/recursos-item5.svg' WHERE `resource_category`.`id` = 5;");
        $this->addSql("UPDATE `resource_category` SET `icon` = '/oit/web/bundles/oit/images/recursos-item7.svg' WHERE `resource_category`.`id` = 6;");
        $this->addSql("UPDATE `resource_category` SET `icon` = '/oit/web/bundles/oit/images/recursos-item8.svg' WHERE `resource_category`.`id` = 7;");




    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE resource_category DROP icon');
    }
}
