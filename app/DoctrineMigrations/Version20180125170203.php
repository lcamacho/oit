<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180125170203 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE strategic_line_activities (strategic_line_id INT NOT NULL, activity_id INT NOT NULL, INDEX IDX_45F6B978EDC0C98A (strategic_line_id), INDEX IDX_45F6B97881C06096 (activity_id), PRIMARY KEY(strategic_line_id, activity_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE strategic_line_activities ADD CONSTRAINT FK_45F6B978EDC0C98A FOREIGN KEY (strategic_line_id) REFERENCES strategic_line (id)');
        $this->addSql('ALTER TABLE strategic_line_activities ADD CONSTRAINT FK_45F6B97881C06096 FOREIGN KEY (activity_id) REFERENCES activity (id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE strategic_line_activities');
    }
}
