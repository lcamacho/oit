<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180125175148 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE action_lines (action_id INT NOT NULL, line_id INT NOT NULL, INDEX IDX_A8CF402A9D32F035 (action_id), INDEX IDX_A8CF402A4D7B7542 (line_id), PRIMARY KEY(action_id, line_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE action_lines ADD CONSTRAINT FK_A8CF402A9D32F035 FOREIGN KEY (action_id) REFERENCES action (id)');
        $this->addSql('ALTER TABLE action_lines ADD CONSTRAINT FK_A8CF402A4D7B7542 FOREIGN KEY (line_id) REFERENCES line (id)');
        $this->addSql('DROP TABLE action_objetives');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE action_objetives (action_id INT NOT NULL, objetive_id INT NOT NULL, INDEX IDX_EF6236429D32F035 (action_id), INDEX IDX_EF623642F849066C (objetive_id), PRIMARY KEY(action_id, objetive_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE action_objetives ADD CONSTRAINT FK_EF6236429D32F035 FOREIGN KEY (action_id) REFERENCES action (id)');
        $this->addSql('ALTER TABLE action_objetives ADD CONSTRAINT FK_EF623642F849066C FOREIGN KEY (objetive_id) REFERENCES objetives (id)');
        $this->addSql('DROP TABLE action_lines');
    }
}
