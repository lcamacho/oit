<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180130224851 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE strategic_line ADD guide_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE strategic_line ADD CONSTRAINT FK_8DF6A041D7ED1D4B FOREIGN KEY (guide_id) REFERENCES implementation_guide (id)');
        $this->addSql('CREATE INDEX IDX_8DF6A041D7ED1D4B ON strategic_line (guide_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE strategic_line DROP FOREIGN KEY FK_8DF6A041D7ED1D4B');
        $this->addSql('DROP INDEX IDX_8DF6A041D7ED1D4B ON strategic_line');
        $this->addSql('ALTER TABLE strategic_line DROP guide_id');
    }
}
