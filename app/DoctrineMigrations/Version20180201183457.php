<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180201183457 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE lines_objetives (line_id INT NOT NULL, objetive_id INT NOT NULL, INDEX IDX_34C58AB94D7B7542 (line_id), INDEX IDX_34C58AB9F849066C (objetive_id), PRIMARY KEY(line_id, objetive_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE lines_objetives ADD CONSTRAINT FK_34C58AB94D7B7542 FOREIGN KEY (line_id) REFERENCES line (id)');
        $this->addSql('ALTER TABLE lines_objetives ADD CONSTRAINT FK_34C58AB9F849066C FOREIGN KEY (objetive_id) REFERENCES objetives (id)');
        $this->addSql('ALTER TABLE line DROP FOREIGN KEY FK_D114B4F6F849066C');
        $this->addSql('DROP INDEX IDX_D114B4F6F849066C ON line');
        $this->addSql('ALTER TABLE line DROP objetive_id');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE lines_objetives');
        $this->addSql('ALTER TABLE line ADD objetive_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE line ADD CONSTRAINT FK_D114B4F6F849066C FOREIGN KEY (objetive_id) REFERENCES objetives (id)');
        $this->addSql('CREATE INDEX IDX_D114B4F6F849066C ON line (objetive_id)');
    }
}
