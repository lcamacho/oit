<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180227172326 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE expected_result (id INT AUTO_INCREMENT NOT NULL, result VARCHAR(255) NOT NULL, indicator VARCHAR(255) NOT NULL, audience INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE strategic_line_allies (strategic_line_id INT NOT NULL, allies_id INT NOT NULL, INDEX IDX_13CCA3DBEDC0C98A (strategic_line_id), INDEX IDX_13CCA3DBF8E0A7D3 (allies_id), PRIMARY KEY(strategic_line_id, allies_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE strategic_line_results (strategic_line_id INT NOT NULL, result_id INT NOT NULL, INDEX IDX_C3CA9A53EDC0C98A (strategic_line_id), INDEX IDX_C3CA9A537A7B643 (result_id), PRIMARY KEY(strategic_line_id, result_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE strategic_line_allies ADD CONSTRAINT FK_13CCA3DBEDC0C98A FOREIGN KEY (strategic_line_id) REFERENCES strategic_line (id)');
        $this->addSql('ALTER TABLE strategic_line_allies ADD CONSTRAINT FK_13CCA3DBF8E0A7D3 FOREIGN KEY (allies_id) REFERENCES allies (id)');
        $this->addSql('ALTER TABLE strategic_line_results ADD CONSTRAINT FK_C3CA9A53EDC0C98A FOREIGN KEY (strategic_line_id) REFERENCES strategic_line (id)');
        $this->addSql('ALTER TABLE strategic_line_results ADD CONSTRAINT FK_C3CA9A537A7B643 FOREIGN KEY (result_id) REFERENCES expected_result (id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE strategic_line_results DROP FOREIGN KEY FK_C3CA9A537A7B643');
        $this->addSql('DROP TABLE expected_result');
        $this->addSql('DROP TABLE strategic_line_allies');
        $this->addSql('DROP TABLE strategic_line_results');
    }
}
