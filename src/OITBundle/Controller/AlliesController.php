<?php

namespace OITBundle\Controller;

use OITBundle\Entity\Allies;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
/**
 * Ally controller.
 *
 * @Route("aliados")
 */
class AlliesController extends Controller {

    /**
     * Lists all ally entities.
     *
     * @Route("/{id}", name="aliados_index")
     * @Method("GET")
     */
    public function indexAction($id) {
        $em = $this->getDoctrine()->getManager();
        
        $implementationGuide = $em->getRepository('OITBundle:ImplementationGuide')->find($id);
        if($implementationGuide->getStatus() == 2){
            return $this->redirectToRoute('index');
        }
        
        $ally = new Allies();
        $form = $this->createForm('OITBundle\Form\AlliesType', $ally);

        $allies = $em->getRepository('OITBundle:Allies')->findBy(array("implementationGuide" => $id));

        return $this->render('allies/index.html.twig', array(
                    'id' => $id,
                    'allies' => $allies,
                    'form' => $form->createView(),
        ));
    }

    /**
     * Creates a new ally entity.
     *
     * @Route("/new", name="aliados_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request) {
        $ally = new Allies();
        $form = $this->createForm('OITBundle\Form\AlliesType', $ally);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($ally);
            $em->flush();

            return $this->redirectToRoute('aliados_show', array('id' => $ally->getId()));
        }

        return $this->render('allies/new.html.twig', array(
                    'ally' => $ally,
                    'form' => $form->createView(),
        ));
    }

    /**
     * Creates a new ally entity.
     *
     * @Route("/ajax", name="aliados_ajax_new")
     * @Method({"POST"})
     */
    public function ajaxAction(Request $request) {
        $ally = new Allies();
        $form = $this->createForm('OITBundle\Form\AlliesType', $ally);
        $form->handleRequest($request);
        $status = false;

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            
            $id = $request->request->get('guide_id');
            $implementationGuide = $em->getRepository('OITBundle:ImplementationGuide')->find($id);
            $ally->setImplementationGuide($implementationGuide);
            
            $em->persist($ally);
            $em->flush();
            $status = true;
        }

        return new JsonResponse(array('status' => $status, "ally" => $ally));
    }

    /**
     * Finds and displays a ally entity.
     *
     * @Route("/{id}", name="aliados_show")
     * @Method("GET")
     */
    public function showAction(Allies $ally) {
        $deleteForm = $this->createDeleteForm($ally);

        return $this->render('allies/show.html.twig', array(
                    'ally' => $ally,
                    'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing ally entity.
     *
     * @Route("/{id}/edit", name="aliados_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Allies $ally) {
        $deleteForm = $this->createDeleteForm($ally);
        $editForm = $this->createForm('OITBundle\Form\AlliesType', $ally);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('aliados_edit', array('id' => $ally->getId()));
        }

        $html = $this->render('allies/edit.html.twig', array(
                    'ally' => $ally,
                    'edit_form' => $editForm->createView()
        ))->getContent();

        return new JsonResponse(array('html' => $html));
    }
    
    /**
     * Displays a form to edit an existing team entity.
     *
     * @Route("/{id}/editAjax", name="aliados_editAjax")
     * @Method({"POST"})
     */
    public function editAjaxAction(Request $request, Allies $ally) {
        $editForm = $this->createForm('OITBundle\Form\AlliesType', $ally);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return new JsonResponse(array('status' => true, 'ally' => $ally));
        }

        return new JsonResponse(array('status' => true, 'ally' => $ally));
    }
    
    /**
     * Deletes a ally entity.
     *
     * @Route("/{id}", name="aliados_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Allies $ally) {
        $form = $this->createDeleteForm($ally);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($ally);
            $em->flush();
        }

        return $this->redirectToRoute('aliados_index');
    }

    /**
     * Creates a form to delete a ally entity.
     *
     * @param Allies $ally The ally entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Allies $ally) {
        return $this->createFormBuilder()
                        ->setAction($this->generateUrl('aliados_delete', array('id' => $ally->getId())))
                        ->setMethod('DELETE')
                        ->getForm()
        ;
    }

}
