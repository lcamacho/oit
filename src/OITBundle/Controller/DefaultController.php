<?php

namespace OITBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use OITBundle\Enum\StatusEnum;

class DefaultController extends Controller {

    /**
     * @Route("/" , name="index")
     * @Template()
     */
    public function indexAction() {
        $em = $this->getDoctrine()->getManager();
        
        $status = StatusEnum::getAvailableStatus();
        
        
        $ImplementationGuide = $em->getRepository('OITBundle:ImplementationGuide')->findAll();
        // return array();
        return $this->render('default/index.html.twig', array(
                    "ImplementationGuides" => $ImplementationGuide,
                    "status" => $status
        ));
    }

    /**
     * @Route("/login" , name="login")
     */
    public function loginAction(Request $request) {
        
        $authUtils = $this->get("security.authentication_utils");
        // get the login error if there is one
        $error = $authUtils->getLastAuthenticationError();

        // last username entered by the user
        $lastUsername = $authUtils->getLastUsername();

        return $this->render('default/login.html.twig', array(
                    'last_username' => $lastUsername,
                    'error' => $error,
        ));
    }
    
    /**
     * @Route("/logout" , name="logout")
     */
    public function logoutAction() {
      
    }

    /**
     * @Route("/sobre-la-politica" , name="sobre-la-politica")
     */
    public function aboutAction() {

        $em = $this->getDoctrine()->getManager();

        $objetives = $em->getRepository('OITBundle:Objetives')->findAll();

        $principles = $em->getRepository('OITBundle:Principles')->findAll();

        $lines = $em->getRepository('OITBundle:Line')->findAll();


        return $this->render('default/about.html.twig', array(
                    "objetives" => $objetives,
                    "principles" => $principles,
                    "lines" => $lines,
        ));
    }
    
    /**
     * @Route("/finalizar/{id}" , name="finish")
     */
    public function finishAction($id) {

        $em = $this->getDoctrine()->getManager();

        $objetives = $em->getRepository('OITBundle:Objetives')->findAll();

        $principles = $em->getRepository('OITBundle:Principles')->findAll();

        $lines = $em->getRepository('OITBundle:Line')->findAll();


        return $this->render('default/finish.html.twig', array("id" => $id));
    }

    /**
     * @Route("/recursos")
     */
    public function resourcesAction() {
        return $this->render('default/resources.html.twig', array());
    }

}
