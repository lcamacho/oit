<?php

namespace OITBundle\Controller;

use OITBundle\Entity\ExpectedResult;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;use Symfony\Component\HttpFoundation\Request;

/**
 * Expectedresult controller.
 *
 * @Route("resultados")
 */
class ExpectedResultController extends Controller
{
    /**
     * Lists all expectedResult entities.
     *
     * @Route("/", name="resultados_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $expectedResults = $em->getRepository('OITBundle:ExpectedResult')->findAll();

        return $this->render('expectedresult/index.html.twig', array(
            'expectedResults' => $expectedResults,
        ));
    }

    /**
     * Creates a new expectedResult entity.
     *
     * @Route("/new", name="resultados_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $expectedResult = new Expectedresult();
        $form = $this->createForm('OITBundle\Form\ExpectedResultType', $expectedResult);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($expectedResult);
            $em->flush();

            return $this->redirectToRoute('resultados_show', array('id' => $expectedResult->getId()));
        }

        return $this->render('expectedresult/new.html.twig', array(
            'expectedResult' => $expectedResult,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a expectedResult entity.
     *
     * @Route("/{id}", name="resultados_show")
     * @Method("GET")
     */
    public function showAction(ExpectedResult $expectedResult)
    {
        $deleteForm = $this->createDeleteForm($expectedResult);

        return $this->render('expectedresult/show.html.twig', array(
            'expectedResult' => $expectedResult,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing expectedResult entity.
     *
     * @Route("/{id}/edit", name="resultados_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, ExpectedResult $expectedResult)
    {
        $deleteForm = $this->createDeleteForm($expectedResult);
        $editForm = $this->createForm('OITBundle\Form\ExpectedResultType', $expectedResult);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('resultados_edit', array('id' => $expectedResult->getId()));
        }

        return $this->render('expectedresult/edit.html.twig', array(
            'expectedResult' => $expectedResult,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a expectedResult entity.
     *
     * @Route("/{id}", name="resultados_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, ExpectedResult $expectedResult)
    {
        $form = $this->createDeleteForm($expectedResult);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($expectedResult);
            $em->flush();
        }

        return $this->redirectToRoute('resultados_index');
    }

    /**
     * Creates a form to delete a expectedResult entity.
     *
     * @param ExpectedResult $expectedResult The expectedResult entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(ExpectedResult $expectedResult)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('resultados_delete', array('id' => $expectedResult->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
