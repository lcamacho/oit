<?php

namespace OITBundle\Controller;

use OITBundle\Entity\ImplementationGuide;
use OITBundle\Entity\TargetAudience;
use OITBundle\Entity\ImprovementByGuide;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use OITBundle\Entity\Team;
use Symfony\Component\HttpFoundation\Response;
use OITBundle\Enum\AudienceEnum;
/**
 * Implementationguide controller.
 *
 * @Route("guias-de-implementacion")
 */
class ImplementationGuideController extends Controller {

    /**
     * Lists all implementationGuide entities.
     *
     * @Route("/", name="guias-de-implementacion_index")
     * @Method("GET")
     */
    public function indexAction() {
        $em = $this->getDoctrine()->getManager();

        $implementationGuides = $em->getRepository('OITBundle:ImplementationGuide')->findAll();

        return $this->render('implementationguide/index.html.twig', array(
                    'implementationGuides' => $implementationGuides,
        ));
    }

    /**
     * Lists all implementationGuide entities.
     *
     * @Route("/complete_guide", name="complete_guide")
     * @Method("POST")
     */
    public function completeAction(Request $request) {
        $em = $this->getDoctrine()->getManager();

        $id = $request->request->get('guide_id');
        $implementationGuides = $em->getRepository('OITBundle:ImplementationGuide')->find($id);
        $implementationGuides->setStatus(2);
        $em->flush();

        return $this->redirectToRoute('index', array('id' => $id));
    }

    /**
     * Creates a new implementationGuide entity.
     *
     * @Route("/nueva", name="guias-de-implementacion_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request) {
        $implementationGuide = new Implementationguide();
        $form = $this->createForm('OITBundle\Form\ImplementationGuideType', $implementationGuide);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $user = $this->get('security.token_storage')->getToken()->getUser();

            $implementationGuide->setAuthor($user);
            $implementationGuide->setStatus(1);
            $em->persist($implementationGuide);
            $em->flush();

            $improvementPoints = $em->getRepository('OITBundle:ImprovementPoint')->findAll();

            foreach ($improvementPoints as $improvementPoint) {
                $improvementByGuide = new ImprovementByGuide();
                $improvementByGuide->setAnswer(null);
                $improvementByGuide->setImplementationGuide($implementationGuide);
                $improvementByGuide->setImprovement($improvementPoint);

                $em->persist($improvementByGuide);
            }

            $audiences = array("Puestos estratégicos", "Puestos tácticos", "Puestos operativos", "Familias", "Aliados", "Personas con VIH de ASAZGUA");
            foreach ($audiences as $audience) {
                $targetAudience = new TargetAudience();
                $targetAudience->setAudience($audience);
                $targetAudience->setImplementationGuide($implementationGuide);

                $em->persist($targetAudience);
            }

            $em->flush();

            return $this->redirectToRoute('equipos_index', array('id' => $implementationGuide->getId()));
        }

        return $this->render('implementationguide/new.html.twig', array(
                    'implementationGuide' => $implementationGuide,
                    'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/exportar/{id}", name="export_implementation_guide")
     * @Method({"GET"})
     */
    public function exportImplementationGuide($id) {

        $em = $this->getDoctrine()->getManager();

        $team = new Team();
        $form = $this->createForm('OITBundle\Form\TeamType', $team);
        $jobs = $form->get('job')->getConfig()->getOption('choices');
        
        $levels = $this->createForm('OITBundle\Form\TargetAudienceType', new TargetAudience())->get('vIHRiskLevel')->getConfig()->getOption('choices');
        

        $teams = $em->getRepository('OITBundle:Team')->findBy(array("implementationGuide" => $id));
        
        $implementationGuide = $em->getRepository('OITBundle:ImplementationGuide')->find($id);
        $improvementPoints = $em->getRepository('OITBundle:ImprovementByGuide')->findBy(array("implementationGuide" => $id));
        $targetAudiences = $em->getRepository('OITBundle:TargetAudience')->findBy(array("implementationGuide" => $id));
        $allies = $em->getRepository('OITBundle:Allies')->findBy(array("implementationGuide" => $id));
        
        $strategicLines1 = $em->getRepository('OITBundle:StrategicLine')->findBy(array("implementationGuide" => $id, "objetive" => 1));
        $strategicLines2 = $em->getRepository('OITBundle:StrategicLine')->findBy(array("implementationGuide" => $id, "objetive" => 2));
        $strategicLines3 = $em->getRepository('OITBundle:StrategicLine')->findBy(array("implementationGuide" => $id, "objetive" => 3));
        $strategicLines4 = $em->getRepository('OITBundle:StrategicLine')->findBy(array("implementationGuide" => $id, "objetive" => 4));
        $strategicLines5 = $em->getRepository('OITBundle:StrategicLine')->findBy(array("implementationGuide" => $id, "objetive" => 5));
        $audiences = AudienceEnum::getAvailableAudience();
        
        $html = $this->render('reports/report_implementation_guide.html.twig', array(
                    'id' => $id,
                    'implementationGuide' => $implementationGuide,
                    'teams' => $teams,
                    'improvementPoints' => $improvementPoints,
                    'targetAudiences' => $targetAudiences,
                    'allies' => $allies,
                    'strategicLines1' => $strategicLines1,
                    'strategicLines2' => $strategicLines2,
                    'strategicLines3' => $strategicLines3,
                    'strategicLines4' => $strategicLines4,
                    'strategicLines5' => $strategicLines5,
                    'jobs' => array_flip($jobs),
                    'levels' => array_flip($levels),
                    'audiences' => $audiences
                ))->getContent();

        $pdfGenerator = $this->get('spraed.pdf.generator');

        return new Response($pdfGenerator->generatePDF($html), 200, array(
            'Content-Type' => 'application/pdf',
            'Content-Disposition' => 'inline; filename="Reporte de programa.pdf"'
                )
        );
    }

    /**
     * Finds and displays a implementationGuide entity.
     *
     * @Route("/{id}", name="guias-de-implementacion_show")
     * @Method("GET")
     */
    public function showAction(ImplementationGuide $implementationGuide) {
        $deleteForm = $this->createDeleteForm($implementationGuide);

        return $this->render('implementationguide/show.html.twig', array(
                    'implementationGuide' => $implementationGuide,
                    'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing implementationGuide entity.
     *
     * @Route("/{id}/edit", name="guias-de-implementacion_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, ImplementationGuide $implementationGuide) {
        $deleteForm = $this->createDeleteForm($implementationGuide);
        $editForm = $this->createForm('OITBundle\Form\ImplementationGuideType', $implementationGuide);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('guias-de-implementacion_edit', array('id' => $implementationGuide->getId()));
        }

        return $this->render('implementationguide/edit.html.twig', array(
                    'implementationGuide' => $implementationGuide,
                    'edit_form' => $editForm->createView(),
                    'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a implementationGuide entity.
     *
     * @Route("/{id}", name="guias-de-implementacion_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, ImplementationGuide $implementationGuide) {
        $form = $this->createDeleteForm($implementationGuide);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($implementationGuide);
            $em->flush();
        }

        return $this->redirectToRoute('guias-de-implementacion_index');
    }

    /**
     * Creates a form to delete a implementationGuide entity.
     *
     * @param ImplementationGuide $implementationGuide The implementationGuide entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(ImplementationGuide $implementationGuide) {
        return $this->createFormBuilder()
                        ->setAction($this->generateUrl('guias-de-implementacion_delete', array('id' => $implementationGuide->getId())))
                        ->setMethod('DELETE')
                        ->getForm()
        ;
    }

}
