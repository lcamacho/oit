<?php

namespace OITBundle\Controller;

use OITBundle\Entity\ImprovementByGuide;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

/**
 * Improvementbyguide controller.
 *
 * @Route("preparando-el-terreno")
 */
class ImprovementByGuideController extends Controller {

    /**
     * Lists all improvementByGuide entities.
     *
     * @Route("/{id}", name="improvementbyguide_index")
     * @Method("GET")
     */
    public function indexAction($id) {
        $em = $this->getDoctrine()->getManager();

        $implementationGuide = $em->getRepository('OITBundle:ImplementationGuide')->find($id);
        if($implementationGuide->getStatus() == 2){
            return $this->redirectToRoute('index');
        }
        
        $improvementPoints = $em->getRepository('OITBundle:ImprovementByGuide')->findBy(array("implementationGuide" => $id));

        return $this->render('improvementbyguide/index.html.twig', array(
                    'improvementPoints' => $improvementPoints,
                    'id' => $id
        ));
    }
    
    /**
     * Lists all improvementByGuide entities.
     *
     * @Route("/{id}/mejoras", name="mejoras_list")
     * @Method("GET")
     */
    public function indexImprovements($id) {
        $em = $this->getDoctrine()->getManager();
  
        $improvementPoints = $em->getRepository('OITBundle:ImprovementByGuide')->findBy(array("implementationGuide" => $id, 'answer' => 0));

        return $this->render('improvementbyguide/improvements.html.twig', array(
                    'improvementPoints' => $improvementPoints,
                    'id' => $id
        ));
    }

    /**
     * Creates a new improvementByGuide entity.
     *
     * @Route("/new", name="improvementbyguide_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request) {
        $improvementByGuide = new Improvementbyguide();
        $form = $this->createForm('OITBundle\Form\ImprovementByGuideType', $improvementByGuide);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($improvementByGuide);
            $em->flush();

            return $this->redirectToRoute('improvementbyguide_show', array('id' => $improvementByGuide->getId()));
        }

        return $this->render('improvementbyguide/new.html.twig', array(
                    'improvementByGuide' => $improvementByGuide,
                    'form' => $form->createView(),
        ));
    }

    /**
     * Creates a new improvementByGuide entity.
     *
     * @Route("/add", name="improvementbyguide_add")
     * @Method({"GET", "POST"})
     */
    public function addAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        
        $improvementPoints = $em->getRepository('OITBundle:ImprovementByGuide')->findBy(array("implementationGuide" => $request->request->get('guide_id')));
        
        for($i = 0; $i < count($improvementPoints); $i++){
            $improvementPoints[$i]->setAnswer($request->request->get('improvement' . ($i+1)));
            $em->flush();
        }
        
        return $this->redirectToRoute("mejoras_list", array("id" =>$request->request->get('guide_id')));
    }

    /**
     * Finds and displays a improvementByGuide entity.
     *
     * @Route("/{id}", name="improvementbyguide_show")
     * @Method("GET")
     */
    public function showAction(ImprovementByGuide $improvementByGuide) {
        $deleteForm = $this->createDeleteForm($improvementByGuide);

        return $this->render('improvementbyguide/show.html.twig', array(
                    'improvementByGuide' => $improvementByGuide,
                    'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing improvementByGuide entity.
     *
     * @Route("/{id}/edit", name="improvementbyguide_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, ImprovementByGuide $improvementByGuide) {
        $deleteForm = $this->createDeleteForm($improvementByGuide);
        $editForm = $this->createForm('OITBundle\Form\ImprovementByGuideType', $improvementByGuide);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('improvementbyguide_edit', array('id' => $improvementByGuide->getId()));
        }

        return $this->render('improvementbyguide/edit.html.twig', array(
                    'improvementByGuide' => $improvementByGuide,
                    'edit_form' => $editForm->createView(),
                    'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a improvementByGuide entity.
     *
     * @Route("/{id}", name="improvementbyguide_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, ImprovementByGuide $improvementByGuide) {
        $form = $this->createDeleteForm($improvementByGuide);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($improvementByGuide);
            $em->flush();
        }

        return $this->redirectToRoute('improvementbyguide_index');
    }

    /**
     * Creates a form to delete a improvementByGuide entity.
     *
     * @param ImprovementByGuide $improvementByGuide The improvementByGuide entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(ImprovementByGuide $improvementByGuide) {
        return $this->createFormBuilder()
                        ->setAction($this->generateUrl('improvementbyguide_delete', array('id' => $improvementByGuide->getId())))
                        ->setMethod('DELETE')
                        ->getForm()
        ;
    }

}
