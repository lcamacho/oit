<?php

namespace OITBundle\Controller;

use OITBundle\Entity\Line;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;use Symfony\Component\HttpFoundation\Request;

/**
 * Line controller.
 *
 * @Route("line")
 */
class LineController extends Controller
{
    /**
     * Lists all line entities.
     *
     * @Route("/", name="line_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $lines = $em->getRepository('OITBundle:Line')->findAll();

        return $this->render('line/index.html.twig', array(
            'lines' => $lines,
        ));
    }

    /**
     * Creates a new line entity.
     *
     * @Route("/new", name="line_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $line = new Line();
        $form = $this->createForm('OITBundle\Form\LineType', $line);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($line);
            $em->flush();

            return $this->redirectToRoute('line_show', array('id' => $line->getId()));
        }

        return $this->render('line/new.html.twig', array(
            'line' => $line,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a line entity.
     *
     * @Route("/{id}", name="line_show")
     * @Method("GET")
     */
    public function showAction(Line $line)
    {
        $deleteForm = $this->createDeleteForm($line);

        return $this->render('line/show.html.twig', array(
            'line' => $line,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing line entity.
     *
     * @Route("/{id}/edit", name="line_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Line $line)
    {
        $deleteForm = $this->createDeleteForm($line);
        $editForm = $this->createForm('OITBundle\Form\LineType', $line);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('line_edit', array('id' => $line->getId()));
        }

        return $this->render('line/edit.html.twig', array(
            'line' => $line,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a line entity.
     *
     * @Route("/{id}", name="line_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Line $line)
    {
        $form = $this->createDeleteForm($line);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($line);
            $em->flush();
        }

        return $this->redirectToRoute('line_index');
    }

    /**
     * Creates a form to delete a line entity.
     *
     * @param Line $line The line entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Line $line)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('line_delete', array('id' => $line->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
