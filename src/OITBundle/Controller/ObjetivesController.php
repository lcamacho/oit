<?php

namespace OITBundle\Controller;

use OITBundle\Entity\Objetives;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;use Symfony\Component\HttpFoundation\Request;

/**
 * Objetive controller.
 *
 * @Route("objetivos")
 */
class ObjetivesController extends Controller
{
    /**
     * Lists all objetive entities.
     *
     * @Route("/", name="objetivos_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $objetives = $em->getRepository('OITBundle:Objetives')->findAll();

        return $this->render('objetives/index.html.twig', array(
            'objetives' => $objetives,
        ));
    }

    /**
     * Creates a new objetive entity.
     *
     * @Route("/new", name="objetivos_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $objetive = new Objetives();
        $form = $this->createForm('OITBundle\Form\ObjetivesType', $objetive);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($objetive);
            $em->flush();

            return $this->redirectToRoute('objetivos_show', array('id' => $objetive->getId()));
        }

        return $this->render('objetives/new.html.twig', array(
            'objetive' => $objetive,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a objetive entity.
     *
     * @Route("/{id}", name="objetivos_show")
     * @Method("GET")
     */
    public function showAction(Objetives $objetive)
    {
        $deleteForm = $this->createDeleteForm($objetive);

        return $this->render('objetives/show.html.twig', array(
            'objetive' => $objetive,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing objetive entity.
     *
     * @Route("/{id}/edit", name="objetivos_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Objetives $objetive)
    {
        $deleteForm = $this->createDeleteForm($objetive);
        $editForm = $this->createForm('OITBundle\Form\ObjetivesType', $objetive);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('objetivos_edit', array('id' => $objetive->getId()));
        }

        return $this->render('objetives/edit.html.twig', array(
            'objetive' => $objetive,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a objetive entity.
     *
     * @Route("/{id}", name="objetivos_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Objetives $objetive)
    {
        $form = $this->createDeleteForm($objetive);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($objetive);
            $em->flush();
        }

        return $this->redirectToRoute('objetivos_index');
    }

    /**
     * Creates a form to delete a objetive entity.
     *
     * @param Objetives $objetive The objetive entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Objetives $objetive)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('objetivos_delete', array('id' => $objetive->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
