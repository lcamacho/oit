<?php

namespace OITBundle\Controller;

use OITBundle\Entity\Resource;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;use Symfony\Component\HttpFoundation\Request;

/**
 * Resource controller.
 *
 * @Route("recursos")
 */
class ResourceController extends Controller
{
    /**
     * Lists all resource entities.
     *
     * @Route("/", name="recursos_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $resources1 = $em->getRepository('OITBundle:Resource')->findBy(array("category" => 1));

        $resources2 = $em->getRepository('OITBundle:Resource')->findBy(array("category" => 2));

        $resources3 = $em->getRepository('OITBundle:Resource')->findBy(array("category" => 3));

        $resources4 = $em->getRepository('OITBundle:Resource')->findBy(array("category" => 4));

        $resources5 = $em->getRepository('OITBundle:Resource')->findBy(array("category" => 5));

        $resources6 = $em->getRepository('OITBundle:Resource')->findBy(array("category" => 6));

        $resources7 = $em->getRepository('OITBundle:Resource')->findBy(array("category" => 7));

         $categories = $em->getRepository('OITBundle:ResourceCategory')->findAll();

        return $this->render('resource/index.html.twig', array(
            'resources1' => $resources1,
            'resources2' => $resources2,
            'resources3' => $resources3,
            'resources4' => $resources4,
            'resources5' => $resources5,
            'resources6' => $resources6,
            'resources7' => $resources7,
            'categories' => $categories,
        ));
    }

    /**
     * Creates a new resource entity.
     *
     * @Route("/new", name="recursos_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $resource = new Resource();
        $form = $this->createForm('OITBundle\Form\ResourceType', $resource);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($resource);
            $em->flush();

            return $this->redirectToRoute('recursos_show', array('id' => $resource->getId()));
        }

        return $this->render('resource/new.html.twig', array(
            'resource' => $resource,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a resource entity.
     *
     * @Route("/{id}", name="recursos_show")
     * @Method("GET")
     */
    public function showAction(Resource $resource)
    {
        $deleteForm = $this->createDeleteForm($resource);

        return $this->render('resource/show.html.twig', array(
            'resource' => $resource,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing resource entity.
     *
     * @Route("/{id}/edit", name="recursos_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Resource $resource)
    {
        $deleteForm = $this->createDeleteForm($resource);
        $editForm = $this->createForm('OITBundle\Form\ResourceType', $resource);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('recursos_edit', array('id' => $resource->getId()));
        }

        return $this->render('resource/edit.html.twig', array(
            'resource' => $resource,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a resource entity.
     *
     * @Route("/{id}", name="recursos_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Resource $resource)
    {
        $form = $this->createDeleteForm($resource);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($resource);
            $em->flush();
        }

        return $this->redirectToRoute('recursos_index');
    }

    /**
     * Creates a form to delete a resource entity.
     *
     * @param Resource $resource The resource entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Resource $resource)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('recursos_delete', array('id' => $resource->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
