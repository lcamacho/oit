<?php

namespace OITBundle\Controller;

use OITBundle\Entity\StrategicLine;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use OITBundle\Entity\ExpectedResult;

/**
 * Strategicline controller.
 *
 * @Route("lineas-estrategicas")
 */
class StrategicLineController extends Controller {

    /**
     * Lists all strategicLine entities.
     *
     * @Route("/{id}", name="lineas-estrategicas_index")
     * @Method("GET")
     */
    public function indexAction($id) {
        $em = $this->getDoctrine()->getManager();
        
        $implementationGuide = $em->getRepository('OITBundle:ImplementationGuide')->find($id);
        if($implementationGuide->getStatus() == 2){
            return $this->redirectToRoute('index');
        }
        
        $strategicLine = new StrategicLine();
        $form = $this->createForm('OITBundle\Form\StrategicLineType', $strategicLine);

        $actions = $em->getRepository('OITBundle:Action')->findAll();
        $activities = $em->getRepository('OITBundle:Activity')->findAll();
        $objetives = $em->getRepository('OITBundle:Objetives')->findAll();
        $lines = $em->getRepository('OITBundle:Line')->findAll();
        $array = array();
        $array2 = array();
        $array3 = array();

        $expectedResult = new ExpectedResult();
        $formER = $this->createForm('OITBundle\Form\ExpectedResultType', $expectedResult);

        foreach ($objetives as $objetive) {
            $array3[$objetive->getId()] = array("id" => $objetive->getId(), "lines" => array());
        }
        foreach ($lines as $line) {
            foreach ($line->getObjetive() as $objetive) {
                array_push($array3[$objetive->getId()]["lines"], $line->getId());
            }
        }

        foreach ($lines as $line) {
            $array[$line->getId()] = array("id" => $line->getId(), "lines" => array());
        }
        foreach ($actions as $action) {
            foreach ($action->getLines() as $line) {
                array_push($array[$line->getId()]["lines"], $action->getId());
            }
        }

        foreach ($actions as $action) {
            $array2[$action->getId()] = array("id" => $action->getId(), "activities" => array());
        }
        foreach ($activities as $activity) {
            foreach ($activity->getActions() as $action) {
                array_push($array2[$action->getId()]["activities"], $activity->getId());
            }
        }

        $strategicLines = $em->getRepository('OITBundle:StrategicLine')->findBy(array("implementationGuide" => $id));

        return $this->render('strategicline/index.html.twig', array(
                    'strategicLines' => $strategicLines,
                    'strategicLine' => $strategicLine,
                    'form' => $form->createView(),
                    'formER' => $formER->createView(),
                    'conditionals' => $array,
                    'conditionals2' => $array2,
                    'conditionals3' => $array3,
                    'objetives' => $objetives,
                    'id' => $id
        ));
    }

    /**
     * Creates a new strategicLine entity.
     *
     * @Route("/ajax", name="lineas-estrategicas_ajax_new")
     * @Method({"POST"})
     */
    public function ajaxAction(Request $request) {
        $strategicLine = new Strategicline();
        $form = $this->createForm('OITBundle\Form\StrategicLineType', $strategicLine);
        $form->handleRequest($request);
        $status = false;

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $id = $request->request->get('guide_id');
            $audiences = $request->request->get('oitbundle_expectedresult')["audience"];
            $results = $request->request->get('oitbundle_expectedresult')["result"];
            $indicators = $request->request->get('oitbundle_expectedresult')["indicator"];

            $array = array();
            for ($i = 0; $i < count($audiences); $i++) {
                $result = new ExpectedResult();
                $result->setAudience($audiences[$i]);
                $result->setResult($results[$i]);
                $result->setIndicator($indicators[$i]);

                $em->persist($result);
                $em->flush();

                array_push($array, $result);
            }

            $strategicLine->setResults($array);

            $implementationGuide = $em->getRepository('OITBundle:ImplementationGuide')->find($id);
            $strategicLine->setImplementationGuide($implementationGuide);

            $em->persist($strategicLine);

            $em->flush();
            $status = true;
        }

        return new JsonResponse(array('status' => $status, "strategicLine" => $strategicLine));
    }

    /**
     * Finds and displays a strategicLine entity.
     *
     * @Route("/{id}", name="lineas-estrategicas_show")
     * @Method("GET")
     */
    public function showAction(StrategicLine $strategicLine) {
        $deleteForm = $this->createDeleteForm($strategicLine);

        return $this->render('strategicline/show.html.twig', array(
                    'actividades' => $strategicLine->getActivities(),
                    'strategicLine' => $strategicLine,
                    'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing strategicLine entity.
     *
     * @Route("/{id}/edit", name="lineas-estrategicas_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, StrategicLine $strategicLine) {
        $editForm = $this->createForm('OITBundle\Form\StrategicLineType', $strategicLine);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('lineas-estrategicas_edit', array('id' => $strategicLine->getId()));
        }

        $em = $this->getDoctrine()->getManager();

        $actions = $em->getRepository('OITBundle:Action')->findAll();
        $activities = $em->getRepository('OITBundle:Activity')->findAll();
        $objetives = $em->getRepository('OITBundle:Objetives')->findAll();
        $lines = $em->getRepository('OITBundle:Line')->findAll();
        $results = $strategicLine->getResults();

        $array = array();
        $array2 = array();
        $array3 = array();

        foreach ($objetives as $objetive) {
            $array3[$objetive->getId()] = array("id" => $objetive->getId(), "lines" => array());
        }
        foreach ($lines as $line) {
            foreach ($line->getObjetive() as $objetive) {
                array_push($array3[$objetive->getId()]["lines"], $line->getId());
            }
        }

        foreach ($lines as $line) {
            $array[$line->getId()] = array("id" => $line->getId(), "lines" => array());
        }
        foreach ($actions as $action) {
            foreach ($action->getLines() as $line) {
                array_push($array[$line->getId()]["lines"], $action->getId());
            }
        }

        foreach ($actions as $action) {
            $array2[$action->getId()] = array("id" => $action->getId(), "activities" => array());
        }
        foreach ($activities as $activity) {
            foreach ($activity->getActions() as $action) {
                array_push($array2[$action->getId()]["activities"], $activity->getId());
            }
        }

        $html = $this->render('strategicline/edit.html.twig', array(
                    'strategicLine' => $strategicLine,
                    'id' => $strategicLine->getId(),
                    'edit_form' => $editForm->createView(),
                    'conditionals' => $array,
                    'conditionals2' => $array2,
                    'conditionals3' => $array3,
                    'objetives' => $objetives,
                    'results' => $results,
                ))->getContent();

        return new JsonResponse(array('html' => $html));
    }

    /**
     * Displays a form to edit an existing team entity.
     *
     * @Route("/{id}/editAjax", name="lineas-estrategicas_editAjax")
     * @Method({"POST"})
     */
    public function editAjaxAction(Request $request, StrategicLine $strategicLine) {
        $editForm = $this->createForm('OITBundle\Form\StrategicLineType', $strategicLine);
        $editForm->handleRequest($request);

        $em = $this->getDoctrine()->getManager();

        $audiences = $request->request->get('oitbundle_expectedresult')["audience"];
        $results = $request->request->get('oitbundle_expectedresult')["result"];
        $indicators = $request->request->get('oitbundle_expectedresult')["indicator"];
        $ids = $request->request->get('oitbundle_expectedresult')["id"];
       
        $array = array();
        for ($i = 0; $i < count($audiences); $i++) {
            if (intval($ids[$i]) != 0) {
                $result = $em->getRepository('OITBundle:ExpectedResult')->find(intval($ids[$i]));
            } else {
                $result = new ExpectedResult();
            }


            $result->setAudience($audiences[$i]);
            $result->setResult($results[$i]);
            $result->setIndicator($indicators[$i]);

            $strategicLine->setResults(null);

            if (intval($ids[$i]) == 0) {
                $em->persist($result);
            }
            
            $em->flush();

            array_push($array, $result);
        }

        $strategicLine->setResults($array);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return new JsonResponse(array('status' => true, 'strategicLine' => $strategicLine));
        }

        return new JsonResponse(array('status' => true, 'strategicLine' => $strategicLine));
    }

    /**
     * Deletes a strategicLine entity.
     *
     * @Route("/{id}", name="lineas-estrategicas_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, StrategicLine $strategicLine) {
        $form = $this->createDeleteForm($strategicLine);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($strategicLine);
            $em->flush();
        }

        return $this->redirectToRoute('lineas-estrategicas_index');
    }

    /**
     * Creates a form to delete a strategicLine entity.
     *
     * @param StrategicLine $strategicLine The strategicLine entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(StrategicLine $strategicLine) {
        return $this->createFormBuilder()
                        ->setAction($this->generateUrl('lineas-estrategicas_delete', array('id' => $strategicLine->getId())))
                        ->setMethod('DELETE')
                        ->getForm()
        ;
    }

}
