<?php

namespace OITBundle\Controller;

use OITBundle\Entity\TargetAudience;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Targetaudience controller.
 *
 * @Route("publicos-meta")
 */
class TargetAudienceController extends Controller {

    /**
     * Lists all targetAudience entities.
     *
     * @Route("/{id}", name="publicos-meta_index")
     * @Method("GET")
     */
    public function indexAction($id) {
        $em = $this->getDoctrine()->getManager();
        
        $implementationGuide = $em->getRepository('OITBundle:ImplementationGuide')->find($id);
        if($implementationGuide->getStatus() == 2){
            return $this->redirectToRoute('index');
        }
        
        $targetAudiences = $em->getRepository('OITBundle:TargetAudience')->findBy(array("implementationGuide" => $id));

        return $this->render('targetaudience/index.html.twig', array(
                    'targetAudiences' => $targetAudiences,
                    'id' => $id
        ));
    }

    /**
     * Creates a new targetAudience entity.
     *
     * @Route("/new", name="publicos-meta_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request) {
        $targetAudience = new Targetaudience();
        $form = $this->createForm('OITBundle\Form\TargetAudienceType', $targetAudience);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($targetAudience);
            $em->flush();

            return $this->redirectToRoute('publicos-meta_show', array('id' => $targetAudience->getId()));
        }

        return $this->render('targetaudience/new.html.twig', array(
                    'targetAudience' => $targetAudience,
                    'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a targetAudience entity.
     *
     * @Route("/{id}", name="publicos-meta_show")
     * @Method("GET")
     */
    public function showAction(TargetAudience $targetAudience) {
        $deleteForm = $this->createDeleteForm($targetAudience);

        return $this->render('targetaudience/show.html.twig', array(
                    'targetAudience' => $targetAudience,
                    'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing targetAudience entity.
     *
     * @Route("/{id}/edit", name="publicos-meta_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, TargetAudience $targetAudience) {
        $editForm = $this->createForm('OITBundle\Form\TargetAudienceType', $targetAudience);
        $editForm->handleRequest($request);

        $html = $this->render('targetaudience/edit.html.twig', array(
                    'targetAudience' => $targetAudience,
                    'edit_form' => $editForm->createView(),
                ))->getContent();

        return new JsonResponse(array('html' => $html));
    }

    /**
     * Displays a form to edit an existing targetAudience entity.
     *
     * @Route("/{id}/editAjax", name="publicos-meta_editAjax")
     * @Method({"POST"})
     */
    public function editAjaxAction(Request $request, TargetAudience $targetAudience) {
        $editForm = $this->createForm('OITBundle\Form\TargetAudienceType', $targetAudience);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();


            $id = $request->request->get('target_id');
            $targetAudience = $this->getDoctrine()->getManager()->getRepository('OITBundle:TargetAudience')->find($id);
            return new JsonResponse(array('status' => true, 'targetAudience' => $targetAudience));
        }

        return new JsonResponse(array('targetAudience' => $targetAudience));
    }

    /**
     * Deletes a targetAudience entity.
     *
     * @Route("/{id}", name="publicos-meta_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, TargetAudience $targetAudience) {
        $form = $this->createDeleteForm($targetAudience);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($targetAudience);
            $em->flush();
        }

        return $this->redirectToRoute('publicos-meta_index');
    }

    /**
     * Creates a form to delete a targetAudience entity.
     *
     * @param TargetAudience $targetAudience The targetAudience entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(TargetAudience $targetAudience) {
        return $this->createFormBuilder()
                        ->setAction($this->generateUrl('publicos-meta_delete', array('id' => $targetAudience->getId())))
                        ->setMethod('DELETE')
                        ->getForm()
        ;
    }

}
