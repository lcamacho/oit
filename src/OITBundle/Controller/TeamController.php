<?php

namespace OITBundle\Controller;

use OITBundle\Entity\Team;
use OITBundle\Entity\ImplementationGuide;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Team controller.
 *
 * @Route("equipos")
 */
class TeamController extends Controller {

    /**
     * Lists all team entities.
     *
     * @Route("/{id}", name="equipos_index")
     * @Method("GET")
     */
    public function indexAction($id) {
        $em = $this->getDoctrine()->getManager();

        $implementationGuide = $em->getRepository('OITBundle:ImplementationGuide')->find($id);
        if($implementationGuide->getStatus() == 2){
            return $this->redirectToRoute('index');
        }
            
        $team = new Team();
        $form = $this->createForm('OITBundle\Form\TeamType', $team);
        $jobs = $form->get('job')->getConfig()->getOption('choices');
        
        $teams = $em->getRepository('OITBundle:Team')->findBy(array("implementationGuide" => $id));

        return $this->render('team/index.html.twig', array(
                    'id' => $id,
                    'teams' => $teams,
                    'team' => $team,
                    'jobs' => array_flip($jobs),
                    'form' => $form->createView(),
        ));
    }

    /**
     * Creates a new team entity.
     *
     * @Route("/new", name="equipos_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request) {
        $team = new Team();
        $form = $this->createForm('OITBundle\Form\TeamType', $team);
        $form->handleRequest($request);


        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $id = $request->request->get('guide_id');
            $implementationGuide = $em->getRepository('OITBundle:ImplementationGuide')->find($id);

            $team->setImplementationGuide($implementationGuide);
            $em->persist($team);
            $em->flush();

            return $this->redirectToRoute('equipos_show', array('id' => $team->getId()));
        }

        return $this->render('team/new.html.twig', array(
                    'team' => $team,
                    'form' => $form->createView(),
        ));
    }

    /**
     * Creates a new team entity.
     *
     * @Route("/ajax", name="equipos_ajax_new")
     * @Method({"POST"})
     */
    public function ajaxAction(Request $request) {
        $team = new Team();
        $form = $this->createForm('OITBundle\Form\TeamType', $team);
        $form->handleRequest($request);
        $status = false;

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            
            $id = $request->request->get('guide_id');
            $implementationGuide = $em->getRepository('OITBundle:ImplementationGuide')->find($id);
            
            $team->setImplementationGuide($implementationGuide);
            $em->persist($team);
            $em->flush();
            $status = true;
        }

        return new JsonResponse(array('status' => $status, "team" => $team));
    }

    /**
     * Finds and displays a team entity.
     *
     * @Route("/{id}", name="equipos_show")
     * @Method("GET")
     */
    public function showAction(Team $team) {
        $deleteForm = $this->createDeleteForm($team);

        return $this->render('team/show.html.twig', array(
                    'team' => $team,
                    'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing team entity.
     *
     * @Route("/{id}/edit", name="equipos_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Team $team) {
        $editForm = $this->createForm('OITBundle\Form\TeamType', $team);
        $editForm->handleRequest($request);
        $jobs = $editForm->get('job')->getConfig()->getOption('choices');
        
        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('equipos_edit', array('id' => $team->getId()));
        }
        $html = $this->render('team/edit.html.twig', array(
                    'team' => $team,
                    'edit_form' => $editForm->createView(), 
                    "jobs" => array_flip($jobs)
                ))->getContent();

        return new JsonResponse(array('html' => $html));
    }

    /**
     * Displays a form to edit an existing team entity.
     *
     * @Route("/{id}/editAjax", name="equipos_editAjax")
     * @Method({"POST"})
     */
    public function editAjaxAction(Request $request, Team $team) {
        $editForm = $this->createForm('OITBundle\Form\TeamType', $team);
        $editForm->handleRequest($request);
        $jobs = $editForm->get('job')->getConfig()->getOption('choices');
        
        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return new JsonResponse(array('status' => true, 'team' => $team, "jobs" => array_flip($jobs)));
        }

        return new JsonResponse(array('status' => true, 'team' => $team));
    }

    /**
     * Deletes a team entity.
     *
     * @Route("/{id}", name="equipos_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Team $team) {
        $form = $this->createDeleteForm($team);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($team);
            $em->flush();
        }

        return $this->redirectToRoute('equipos_index');
    }

    /**
     * Creates a form to delete a team entity.
     *
     * @param Team $team The team entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Team $team) {
        return $this->createFormBuilder()
                        ->setAction($this->generateUrl('equipos_delete', array('id' => $team->getId())))
                        ->setMethod('DELETE')
                        ->getForm()
        ;
    }

}
