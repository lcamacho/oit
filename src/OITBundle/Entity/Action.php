<?php

namespace OITBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JsonSerializable;
/**
 * Action
 *
 * @ORM\Table(name="action")
 * @ORM\Entity(repositoryClass="OITBundle\Repository\ActionRepository")
 */
class Action implements JsonSerializable {  
    
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=255)
     */
    private $description;

    /**
     * Many User have Many Phonenumbers.
     * @ORM\ManyToMany(targetEntity="Line")
     * @ORM\JoinTable(name="action_lines",
     *      joinColumns={@ORM\JoinColumn(name="action_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="line_id", referencedColumnName="id")})
     */
    private $lines;
    
    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Action
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }
    
    public function __toString() {
        return $this->description;
    }
    function getLines() {
        return $this->lines;
    }

    function setLines($lines) {
        $this->lines = $lines;
    }

    public function jsonSerialize() {
        return array(
            'id' => $this->id,
            'description' => $this->description
        );
    }
}

