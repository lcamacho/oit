<?php

namespace OITBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Activity
 *
 * @ORM\Table(name="activity")
 * @ORM\Entity(repositoryClass="OITBundle\Repository\ActivityRepository")
 */
class Activity {

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=355)
     */
    private $description;

    /**
     * Many User have Many Phonenumbers.
     * @ORM\ManyToMany(targetEntity="Action")
     * @ORM\JoinTable(name="activities_actions",
     *      joinColumns={@ORM\JoinColumn(name="activity_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="action_id", referencedColumnName="id")})
     */
    private $actions;

    /**
     * Get id
     *
     * @return int
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Activity
     */
    public function setDescription($description) {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription() {
        return $this->description;
    }

    function getActions() {
        return $this->actions;
    }

    function setActions($actions) {
        $this->actions = $actions;
    }

    public function __toString() {
        return $this->description;
    }

}
