<?php

namespace OITBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JsonSerializable;

/**
 * Allies
 *
 * @ORM\Table(name="allies")
 * @ORM\Entity(repositoryClass="OITBundle\Repository\AlliesRepository")
 */
class Allies implements JsonSerializable {

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     * @ORM\ManyToOne(targetEntity="ImplementationGuide")
     * @ORM\JoinColumn(name="guide_id", referencedColumnName="id")
     */
    private $implementationGuide;

    /**
     * @var string
     *
     * @ORM\Column(name="company", type="string", length=150)
     */
    private $company;

    /**
     * @var string
     *
     * @ORM\Column(name="personOfContact", type="string", length=255)
     */
    private $personOfContact;

    /**
     * @var string
     *
     * @ORM\Column(name="phone", type="string", length=100, nullable=true)
     */
    private $phone;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=100, nullable=true)
     */
    private $email;

    /**
     * Get id
     *
     * @return int
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set company
     *
     * @param string $company
     *
     * @return Allies
     */
    public function setCompany($company) {
        $this->company = $company;

        return $this;
    }

    /**
     * Get company
     *
     * @return string
     */
    public function getCompany() {
        return $this->company;
    }

    /**
     * Set personOfContact
     *
     * @param string $personOfContact
     *
     * @return Allies
     */
    public function setPersonOfContact($personOfContact) {
        $this->personOfContact = $personOfContact;

        return $this;
    }

    /**
     * Get personOfContact
     *
     * @return string
     */
    public function getPersonOfContact() {
        return $this->personOfContact;
    }

    /**
     * Set phone
     *
     * @param string $phone
     *
     * @return Allies
     */
    public function setPhone($phone) {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get phone
     *
     * @return string
     */
    public function getPhone() {
        return $this->phone;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return Allies
     */
    public function setEmail($email) {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail() {
        return $this->email;
    }

    function getImplementationGuide() {
        return $this->implementationGuide;
    }

    function setImplementationGuide($implementationGuide) {
        $this->implementationGuide = $implementationGuide;
    }
    
    public function __toString() {
        return $this->personOfContact;
    }

    public function jsonSerialize() {
        return array(
            'id' => $this->id,
            'company' => $this->company,
            'personOfContact' => $this->personOfContact,
            'phone' => $this->phone,
            'email' => $this->email,
        );
    }

}
