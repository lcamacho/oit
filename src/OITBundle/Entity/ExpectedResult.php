<?php

namespace OITBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use OITBundle\Enum\AudienceEnum;
/**
 * ExpectedResult
 *
 * @ORM\Table(name="expected_result")
 * @ORM\Entity(repositoryClass="OITBundle\Repository\ExpectedResultRepository")
 */
class ExpectedResult
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="result", type="string", length=255)
     */
    private $result;

    /**
     * @var string
     *
     * @ORM\Column(name="indicator", type="string", length=255)
     */
    private $indicator;
    
    /**
     * @var int
     *
     * @ORM\Column(name="audience", type="integer")
     */
    private $audience;
    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set result
     *
     * @param string $result
     *
     * @return ExpectedResult
     */
    public function setResult($result)
    {
        $this->result = $result;

        return $this;
    }

    /**
     * Get result
     *
     * @return string
     */
    public function getResult()
    {
        return $this->result;
    }

    /**
     * Set indicator
     *
     * @param string $indicator
     *
     * @return ExpectedResult
     */
    public function setIndicator($indicator)
    {
        $this->indicator = $indicator;

        return $this;
    }

    /**
     * Get indicator
     *
     * @return string
     */
    public function getIndicator()
    {
        return $this->indicator;
    }
    
    function getAudience() {
        return $this->audience;
    }

    function setAudience($audience) {
        if (!in_array($audience, AudienceEnum::getAvailableTypes())) {
            throw new \InvalidArgumentException("Invalid type");
        }
        
        $this->audience = $audience;

        return $this;
    }


}

