<?php

namespace OITBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use OITBundle\Enum\StatusEnum;
/**
 * ImplementationGuide
 *
 * @ORM\Table(name="implementation_guide")
 * @ORM\Entity(repositoryClass="OITBundle\Repository\ImplementationGuideRepository")
 */
class ImplementationGuide {

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="startDate", type="date")
     */
    private $startDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="endDate", type="date")
     */
    private $endDate;

    /**
     * @var string
     *
     * @ORM\Column(name="personInCharge", type="string", length=100)
     */
    private $personInCharge;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=100)
     */
    private $email;

    /**
     * One Product has One Shipment.
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="author", referencedColumnName="id")
     */
    private $author;

    /**
     * @var int
     *
     * @ORM\Column(name="status", type="integer")
     */
    private $status;

    /**
     * Get id
     *
     * @return int
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set startDate
     *
     * @param \DateTime $startDate
     *
     * @return ImplementationGuide
     */
    public function setStartDate($startDate) {
        $this->startDate = $startDate;

        return $this;
    }

    /**
     * Get startDate
     *
     * @return \DateTime
     */
    public function getStartDate() {
        return $this->startDate;
    }

    /**
     * Set endDate
     *
     * @param \DateTime $endDate
     *
     * @return ImplementationGuide
     */
    public function setEndDate($endDate) {
        $this->endDate = $endDate;

        return $this;
    }

    /**
     * Get endDate
     *
     * @return \DateTime
     */
    public function getEndDate() {
        return $this->endDate;
    }

    /**
     * Set personInCharge
     *
     * @param string $personInCharge
     *
     * @return ImplementationGuide
     */
    public function setPersonInCharge($personInCharge) {
        $this->personInCharge = $personInCharge;

        return $this;
    }

    /**
     * Get personInCharge
     *
     * @return string
     */
    public function getPersonInCharge() {
        return $this->personInCharge;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return ImplementationGuide
     */
    public function setEmail($email) {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail() {
        return $this->email;
    }

    /**
     * Set author
     *
     * @param integer $author
     *
     * @return ImplementationGuide
     */
    public function setAuthor($author) {
        $this->author = $author;

        return $this;
    }

    /**
     * Get author
     *
     * @return int
     */
    public function getAuthor() {
        return $this->author;
    }

    /**
     * Set status
     *
     * @param integer $status
     *
     * @return ImplementationGuide
     */
    public function setStatus($status) {
        if (!in_array($status, StatusEnum::getAvailableTypes())) {
            throw new \InvalidArgumentException("Invalid type");
        }
        
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return int
     */
    public function getStatus() {
        return $this->status;
    }

    public function __toString() {
        return $this->email;
    }

}
