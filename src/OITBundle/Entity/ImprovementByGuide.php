<?php

namespace OITBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ImprovementByGuide
 *
 * @ORM\Table(name="improvement_by_guide")
 * @ORM\Entity(repositoryClass="OITBundle\Repository\ImprovementByGuideRepository")
 */
class ImprovementByGuide {

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="answer", type="integer", nullable=true)
     */
    private $answer;

    /**
     * @var int
     * @ORM\ManyToOne(targetEntity="ImplementationGuide")
     * @ORM\JoinColumn(name="guide_id", referencedColumnName="id")
     */
    private $implementationGuide;
    
    /**
     * @var int
     * @ORM\ManyToOne(targetEntity="ImprovementPoint")
     * @ORM\JoinColumn(name="improvement_id", referencedColumnName="id")
     */
    private $improvement;

    /**
     * Get id
     *
     * @return int
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set answer
     *
     * @param integer $answer
     *
     * @return ImprovementByGuide
     */
    public function setAnswer($answer) {
        $this->answer = $answer;

        return $this;
    }

    /**
     * Get answer
     *
     * @return int
     */
    public function getAnswer() {
        return $this->answer;
    }
    
    function getImplementationGuide() {
        return $this->implementationGuide;
    }

    function getImprovement() {
        return $this->improvement;
    }

    function setImplementationGuide($implementationGuide) {
        $this->implementationGuide = $implementationGuide;
    }

    function setImprovement($improvement) {
        $this->improvement = $improvement;
    }



}
