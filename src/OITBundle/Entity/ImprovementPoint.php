<?php

namespace OITBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ImprovementPoint
 *
 * @ORM\Table(name="improvement_point")
 * @ORM\Entity(repositoryClass="OITBundle\Repository\ImprovementPointRepository")
 */
class ImprovementPoint
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=255)
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="improvement", type="string", length=255)
     */
    private $improvement;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return ImprovementPoint
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set improvement
     *
     * @param string $improvement
     *
     * @return ImprovementPoint
     */
    public function setImprovement($improvement)
    {
        $this->improvement = $improvement;

        return $this;
    }

    /**
     * Get improvement
     *
     * @return string
     */
    public function getImprovement()
    {
        return $this->improvement;
    }
    
    public function __toString() {
        return $this->description;
    }
}

