<?php

namespace OITBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JsonSerializable;

/**
 * Line
 *
 * @ORM\Table(name="line")
 * @ORM\Entity(repositoryClass="OITBundle\Repository\LineRepository")
 */
class Line implements JsonSerializable {

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=255)
     */
    private $description;
    
    /**
     * Many User have Many Phonenumbers.
     * @ORM\ManyToMany(targetEntity="Objetives")
     * @ORM\JoinTable(name="lines_objetives",
     *      joinColumns={@ORM\JoinColumn(name="line_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="objetive_id", referencedColumnName="id")})
     */
    private $objetive;
    
    /**
     * Get id
     *
     * @return int
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Line
     */
    public function setDescription($description) {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription() {
        return $this->description;
    }

    function getObjetive() {
        return $this->objetive;
    }

    function setObjetive($objetive) {
        $this->objetive = $objetive;
    }

    public function __toString() {
        return $this->description;
    }

    public function jsonSerialize() {
        return array(
            'id' => $this->id,
            'description' => $this->description
        );
    }

}
