<?php

namespace OITBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JsonSerializable;

/**
 * StrategicLine
 *
 * @ORM\Table(name="strategic_line")
 * @ORM\Entity(repositoryClass="OITBundle\Repository\StrategicLineRepository")
 */
class StrategicLine implements JsonSerializable {
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @var int
     * @ORM\ManyToOne(targetEntity="ImplementationGuide")
     * @ORM\JoinColumn(name="guide_id", referencedColumnName="id")
     */
    private $implementationGuide;
    
        /**
     * Many Features have One Product.
     * @ORM\ManyToOne(targetEntity="Objetives", inversedBy="lines")
     * @ORM\JoinColumn(name="objetive_id", referencedColumnName="id")
     */
    private $objetive;
    
    /**
     * One Product has One Shipment.
     * @ORM\ManyToOne(targetEntity="Line")
     * @ORM\JoinColumn(name="line", referencedColumnName="id")
     */
    private $line;

    /**
     * One Product has One Shipment.
     * @ORM\ManyToOne(targetEntity="Action")
     * @ORM\JoinColumn(name="action", referencedColumnName="id")
     */
    private $action;

    /**
     * Many User have Many Phonenumbers.
     * @ORM\ManyToMany(targetEntity="Activity")
     * @ORM\JoinTable(name="strategic_line_activities",
     *      joinColumns={@ORM\JoinColumn(name="strategic_line_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="activity_id", referencedColumnName="id")})
     */
    private $activities;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="startDate", type="date")
     */
    private $startDate;

    /**
     * @var string
     *
     * @ORM\Column(name="endDate", type="date")
     */
    private $endDate;

    /**
     * @var string
     *
     * @ORM\Column(name="VerificationSources", type="string", length=255)
     */
    private $verificationSources;

    /**
     * @var string
     *
     * @ORM\Column(name="Media", type="string", length=255)
     */
    private $media;

    /**
     * @var string
     *
     * @ORM\Column(name="budget", type="string", length=50)
     */
    private $budget;
    
    /**
     * Many User have Many Phonenumbers.
     * @ORM\ManyToMany(targetEntity="Allies")
     * @ORM\JoinTable(name="strategic_line_allies",
     *      joinColumns={@ORM\JoinColumn(name="strategic_line_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="allies_id", referencedColumnName="id")})
     */
    private $allies;
    
    /**
     * Many User have Many Phonenumbers.
     * @ORM\ManyToMany(targetEntity="ExpectedResult")
     * @ORM\JoinTable(name="strategic_line_results",
     *      joinColumns={@ORM\JoinColumn(name="strategic_line_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="result_id", referencedColumnName="id")})
     */
    private $results;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set strategicLine
     *
     * @param integer $strategicLine
     *
     * @return StrategicLine
     */
    public function setLine($line)
    {
        $this->line = $line;

        return $this;
    }

    /**
     * Get strategicLine
     *
     * @return int
     */
    public function getLine()
    {
        return $this->line;
    }

    /**
     * Set action
     *
     * @param integer $action
     *
     * @return StrategicLine
     */
    public function setAction($action)
    {
        $this->action = $action;

        return $this;
    }

    /**
     * Get action
     *
     * @return int
     */
    public function getAction()
    {
        return $this->action;
    }

    /**
     * Set activities
     *
     * @param integer $activities
     *
     * @return StrategicLine
     */
    public function setActivities($activities)
    {
        $this->activities = $activities;

        return $this;
    }

    /**
     * Get activities
     *
     * @return int
     */
    public function getActivities()
    {
        return $this->activities;
    }

    /**
     * Set startDate
     *
     * @param \DateTime $startDate
     *
     * @return StrategicLine
     */
    public function setStartDate($startDate)
    {
        $this->startDate = $startDate;

        return $this;
    }

    /**
     * Get startDate
     *
     * @return \DateTime
     */
    public function getStartDate()
    {
        return $this->startDate;
    }

    /**
     * Set endDate
     *
     * @param string $endDate
     *
     * @return StrategicLine
     */
    public function setEndDate($endDate)
    {
        $this->endDate = $endDate;

        return $this;
    }

    /**
     * Get endDate
     *
     * @return string
     */
    public function getEndDate()
    {
        return $this->endDate;
    }

    /**
     * Set verificationSources
     *
     * @param string $verificationSources
     *
     * @return StrategicLine
     */
    public function setVerificationSources($verificationSources)
    {
        $this->verificationSources = $verificationSources;

        return $this;
    }

    /**
     * Get verificationSources
     *
     * @return string
     */
    public function getVerificationSources()
    {
        return $this->verificationSources;
    }

    /**
     * Set media
     *
     * @param string $media
     *
     * @return StrategicLine
     */
    public function setMedia($media)
    {
        $this->media = $media;

        return $this;
    }

    /**
     * Get media
     *
     * @return string
     */
    public function getMedia()
    {
        return $this->media;
    }

    /**
     * Set budget
     *
     * @param string $budget
     *
     * @return StrategicLine
     */
    public function setBudget($budget)
    {
        $this->budget = $budget;

        return $this;
    }

    /**
     * Get budget
     *
     * @return string
     */
    public function getBudget()
    {
        return $this->budget;
    }
    
    function getImplementationGuide() {
        return $this->implementationGuide;
    }

    function setImplementationGuide($implementationGuide) {
        $this->implementationGuide = $implementationGuide;
    }
    
    function getObjetive() {
        return $this->objetive;
    }

    function setObjetive($objetive) {
        $this->objetive = $objetive;
    }
    
    function getAllies() {
        return $this->allies;
    }

    function setAllies($allies) {
        $this->allies = $allies;
    }
    
    function getResults() {
        return $this->results;
    }

    function setResults($results) {
        $this->results = $results;
    }
            
    public function jsonSerialize() {
        return array(
            'id' => $this->id,
            'action' => $this->action,
            'line' => $this->line,
        );
    }
}

