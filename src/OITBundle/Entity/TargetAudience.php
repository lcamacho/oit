<?php

namespace OITBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JsonSerializable;

/**
 * TargetAudience
 *
 * @ORM\Table(name="target_audience")
 * @ORM\Entity(repositoryClass="OITBundle\Repository\TargetAudienceRepository")
 */
class TargetAudience implements JsonSerializable {

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     * @ORM\ManyToOne(targetEntity="ImplementationGuide")
     * @ORM\JoinColumn(name="guide_id", referencedColumnName="id")
     */
    private $implementationGuide;

    /**
     * @var string
     *
     * @ORM\Column(name="audience", type="string", length=50)
     */
    private $audience;

    /**
     * @var string
     *
     * @ORM\Column(name="sector", type="string", length=150, nullable=true)
     */
    private $sector;

    /**
     * @var int
     *
     * @ORM\Column(name="numberOfMens", type="integer", nullable=true)
     */
    private $numberOfMens;

    /**
     * @var int
     *
     * @ORM\Column(name="numberOfWomens", type="integer", nullable=true)
     */
    private $numberOfWomens;

    /**
     * @var int
     *
     * @ORM\Column(name="minAgeRange", type="integer", nullable=true)
     */
    private $minAgeRange;

    /**
     * @var int
     *
     * @ORM\Column(name="maxAgeRange", type="integer", nullable=true)
     */
    private $maxAgeRange;

    /**
     * @var string
     *
     * @ORM\Column(name="profile", type="string", length=255, nullable=true)
     */
    private $profile;

    /**
     * @var string
     *
     * @ORM\Column(name="communicationSkills", type="string", length=255, nullable=true)
     */
    private $communicationSkills;

    /**
     * @var int
     *
     * @ORM\Column(name="VIHRiskLevel", type="integer", nullable=true)
     */
    private $vIHRiskLevel;

    /**
     * Get id
     *
     * @return int
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set audience
     *
     * @param string $audience
     *
     * @return TargetAudience
     */
    public function setAudience($audience) {
        $this->audience = $audience;

        return $this;
    }

    /**
     * Get audience
     *
     * @return string
     */
    public function getAudience() {
        return $this->audience;
    }

    /**
     * Set sector
     *
     * @param string $sector
     *
     * @return TargetAudience
     */
    public function setSector($sector) {
        $this->sector = $sector;

        return $this;
    }

    /**
     * Get sector
     *
     * @return string
     */
    public function getSector() {
        return $this->sector;
    }

    /**
     * Set numberOfMens
     *
     * @param integer $numberOfMens
     *
     * @return TargetAudience
     */
    public function setNumberOfMens($numberOfMens) {
        $this->numberOfMens = $numberOfMens;

        return $this;
    }

    /**
     * Get numberOfMens
     *
     * @return int
     */
    public function getNumberOfMens() {
        return $this->numberOfMens;
    }

    /**
     * Set numberOfWomens
     *
     * @param integer $numberOfWomens
     *
     * @return TargetAudience
     */
    public function setNumberOfWomens($numberOfWomens) {
        $this->numberOfWomens = $numberOfWomens;

        return $this;
    }

    /**
     * Get numberOfWomens
     *
     * @return int
     */
    public function getNumberOfWomens() {
        return $this->numberOfWomens;
    }

    /**
     * Set minAgeRange
     *
     * @param integer $minAgeRange
     *
     * @return TargetAudience
     */
    public function setMinAgeRange($minAgeRange) {
        $this->minAgeRange = $minAgeRange;

        return $this;
    }

    /**
     * Get minAgeRange
     *
     * @return int
     */
    public function getMinAgeRange() {
        return $this->minAgeRange;
    }

    /**
     * Set maxAgeRange
     *
     * @param integer $maxAgeRange
     *
     * @return TargetAudience
     */
    public function setMaxAgeRange($maxAgeRange) {
        $this->maxAgeRange = $maxAgeRange;

        return $this;
    }

    /**
     * Get maxAgeRange
     *
     * @return int
     */
    public function getMaxAgeRange() {
        return $this->maxAgeRange;
    }

    /**
     * Set profile
     *
     * @param string $profile
     *
     * @return TargetAudience
     */
    public function setProfile($profile) {
        $this->profile = $profile;

        return $this;
    }

    /**
     * Get profile
     *
     * @return string
     */
    public function getProfile() {
        return $this->profile;
    }

    /**
     * Set communicationSkills
     *
     * @param string $communicationSkills
     *
     * @return TargetAudience
     */
    public function setCommunicationSkills($communicationSkills) {
        $this->communicationSkills = $communicationSkills;

        return $this;
    }

    /**
     * Get communicationSkills
     *
     * @return string
     */
    public function getCommunicationSkills() {
        return $this->communicationSkills;
    }

    /**
     * Set vIHRiskLevel
     *
     * @param integer $vIHRiskLevel
     *
     * @return TargetAudience
     */
    public function setVIHRiskLevel($vIHRiskLevel) {
        $this->vIHRiskLevel = $vIHRiskLevel;

        return $this;
    }

    /**
     * Get vIHRiskLevel
     *
     * @return int
     */
    public function getVIHRiskLevel() {
        return $this->vIHRiskLevel;
    }

    function getImplementationGuide() {
        return $this->implementationGuide;
    }

    function setImplementationGuide($implementationGuide) {
        $this->implementationGuide = $implementationGuide;
    }

    public function jsonSerialize() {
        return array(
            'id' => $this->id,
            'maxAgeRange' => $this->maxAgeRange,
            'minAgeRange' => $this->minAgeRange,
            'numberOfMens' => $this->numberOfMens,
            'numberOfWomens' => $this->numberOfWomens,
            'audience' => $this->audience,
        );
    }

}
