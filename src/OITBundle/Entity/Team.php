<?php

namespace OITBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JsonSerializable;

/**
 * Team
 *
 * @ORM\Table(name="team")
 * @ORM\Entity(repositoryClass="OITBundle\Repository\TeamRepository")
 */
class Team implements JsonSerializable {

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     * @ORM\ManyToOne(targetEntity="ImplementationGuide")
     * @ORM\JoinColumn(name="guide_id", referencedColumnName="id")
     */
    private $implementationGuide;

    /**
     * @var int
     *
     * @ORM\Column(name="job", type="integer")
     */
    private $job;

    /**
     * @var string
     *
     * @ORM\Column(name="fullName", type="string", length=150)
     */
    private $fullName;

    /**
     * @var string
     *
     * @ORM\Column(name="phone", type="string", length=30, nullable=true)
     */
    private $phone;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=50, nullable=true)
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="Skype", type="string", length=50, nullable=true)
     */
    private $skype;

    /**
     * @var string
     *
     * @ORM\Column(name="ExperienceInHIV", type="string", length=255, nullable=true)
     */
    private $experienceInHIV;

    /**
     * @var string
     *
     * @ORM\Column(name="formtiveNeeds", type="string", length=255)
     */
    private $formtiveNeeds;

    /**
     * @var string
     *
     * @ORM\Column(name="supportTeam", type="string", length=255)
     */
    private $supportTeam;

    /**
     * @var string
     *
     * @ORM\Column(name="phrase", type="string", length=255)
     */
    private $phrase;

    /**
     * Many User have Many Phonenumbers.
     * @ORM\ManyToMany(targetEntity="Objetives")
     * @ORM\JoinTable(name="guide_objetives",
     *      joinColumns={@ORM\JoinColumn(name="guide_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="objetive_id", referencedColumnName="id")})
     */
    private $objetives;

    /**
     * Many User have Many Phonenumbers.
     * @ORM\ManyToMany(targetEntity="Principles")
     * @ORM\JoinTable(name="guide_principles",
     *      joinColumns={@ORM\JoinColumn(name="guide_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="principle_id", referencedColumnName="id")})
     */
    private $principles;

    /**
     * Many User have Many Phonenumbers.
     * @ORM\ManyToMany(targetEntity="Task")
     * @ORM\JoinTable(name="guide_tasks",
     *      joinColumns={@ORM\JoinColumn(name="guide_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="task_id", referencedColumnName="id")})
     */
    private $tasks;

    /**
     * Get id
     *
     * @return int
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set job
     *
     * @param integer $job
     *
     * @return Team
     */
    public function setJob($job) {
        $this->job = $job;

        return $this;
    }

    /**
     * Get job
     *
     * @return int
     */
    public function getJob() {
        return $this->job;
    }

    /**
     * Set fullName
     *
     * @param string $fullName
     *
     * @return Team
     */
    public function setFullName($fullName) {
        $this->fullName = $fullName;

        return $this;
    }

    /**
     * Get fullName
     *
     * @return string
     */
    public function getFullName() {
        return $this->fullName;
    }

    /**
     * Set phone
     *
     * @param string $phone
     *
     * @return Team
     */
    public function setPhone($phone) {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get phone
     *
     * @return string
     */
    public function getPhone() {
        return $this->phone;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return Team
     */
    public function setEmail($email) {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail() {
        return $this->email;
    }

    /**
     * Set skype
     *
     * @param string $skype
     *
     * @return Team
     */
    public function setSkype($skype) {
        $this->skype = $skype;

        return $this;
    }

    /**
     * Get skype
     *
     * @return string
     */
    public function getSkype() {
        return $this->skype;
    }

    /**
     * Set experienceInHIV
     *
     * @param string $experienceInHIV
     *
     * @return Team
     */
    public function setExperienceInHIV($experienceInHIV) {
        $this->experienceInHIV = $experienceInHIV;

        return $this;
    }

    /**
     * Get experienceInHIV
     *
     * @return string
     */
    public function getExperienceInHIV() {
        return $this->experienceInHIV;
    }

    /**
     * Set formtiveNeeds
     *
     * @param string $formtiveNeeds
     *
     * @return Team
     */
    public function setFormtiveNeeds($formtiveNeeds) {
        $this->formtiveNeeds = $formtiveNeeds;

        return $this;
    }

    /**
     * Get formtiveNeeds
     *
     * @return string
     */
    public function getFormtiveNeeds() {
        return $this->formtiveNeeds;
    }

    function getSupportTeam() {
        return $this->supportTeam;
    }

    function getPhrase() {
        return $this->phrase;
    }

    function setSupportTeam($supportTeam) {
        $this->supportTeam = $supportTeam;
    }

    function setPhrase($phrase) {
        $this->phrase = $phrase;
    }

    function getObjetives() {
        return $this->objetives;
    }

    function getPrinciples() {
        return $this->principles;
    }

    function getTasks() {
        return $this->tasks;
    }

    function setObjetives($objetives) {
        $this->objetives = $objetives;
    }

    function setPrinciples($principles) {
        $this->principles = $principles;
    }

    function setTasks($tasks) {
        $this->tasks = $tasks;
    }

    function getImplementationGuide() {
        return $this->implementationGuide;
    }

    function setImplementationGuide($implementationGuide) {
        $this->implementationGuide = $implementationGuide;
    }

    public function jsonSerialize() {
        return array(
            'id' => $this->id,
            'job' => $this->job,
            'fullName' => $this->fullName,
        );
    }

}
