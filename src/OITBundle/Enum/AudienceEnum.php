<?php
namespace OITBundle\Enum;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of StatusEnum
 *
 * @author Eldomo-info
 */

abstract class AudienceEnum
{
    const STRATEGICS = 1;
    const TACTICS = 2;
    const OPERATIVES = 3;
    const FAMILIES = 4;
    const ALLIES = 5;
    const VIH = 6;

    /** @var array user friendly named type */
    protected static $typeName = [
        self::STRATEGICS => 'Puestos estratégicos',
        self::TACTICS => 'Puestos tácticos',
        self::OPERATIVES => 'Puestos operativos',
        self::FAMILIES => 'Familias',
        self::ALLIES => 'Aliados',
        self::VIH => 'Personas con VIH de ASAZGUA',
        
    ];

    /**
     * @param  string $typeShortName
     * @return string
     */
    public static function getTypeName($typeShortName)
    {
        if (!isset(static::$typeName[$typeShortName])) {
            return "Unknown type ($typeShortName)";
        }

        return static::$typeName[$typeShortName];
    }

    /**
     * @return array<string>
     */
    public static function getAvailableTypes()
    {
        return [
            self::STRATEGICS,
            self::TACTICS,
            self::OPERATIVES,
            self::FAMILIES,
            self::ALLIES,
            self::VIH,
        ];
    }
    /**
     * @return array<string>
     */
    public static function getAvailableAudience()
    {
        return [
            self::STRATEGICS => 'Puestos estratégicos',
            self::TACTICS => 'Puestos tácticos',
            self::OPERATIVES => 'Puestos operativos',
            self::FAMILIES => 'Familias',
            self::ALLIES => 'Aliados',
            self::VIH => 'Personas con VIH de ASAZGUA',
        ];
    }
}