<?php
namespace OITBundle\Enum;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of StatusEnum
 *
 * @author Eldomo-info
 */

abstract class StatusEnum
{
    const IN_PROCESS = 1;
    const FINISHED = 2;

    /** @var array user friendly named type */
    protected static $typeName = [
        self::IN_PROCESS => 'En proceso',
        self::FINISHED => 'Finalizado',
    ];

    /**
     * @param  string $typeShortName
     * @return string
     */
    public static function getTypeName($typeShortName)
    {
        if (!isset(static::$typeName[$typeShortName])) {
            return "Unknown type ($typeShortName)";
        }

        return static::$typeName[$typeShortName];
    }

    /**
     * @return array<string>
     */
    public static function getAvailableTypes()
    {
        return [
            self::IN_PROCESS,
            self::FINISHED,
        ];
    }
    /**
     * @return array<string>
     */
    public static function getAvailableStatus()
    {
        return [
            self::IN_PROCESS => 'En proceso',
            self::FINISHED => 'Finalizado',
        ];
    }
}