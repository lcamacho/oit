<?php

namespace OITBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use OITBundle\Enum\AudienceEnum;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class ExpectedResultType extends AbstractType {

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder->add('result', TextareaType::class, array(
                    'attr' => array('rows' => '5')))
                ->add('indicator', TextareaType::class, array(
                    'attr' => array('rows' => '5')))
                ->add('audience', ChoiceType::class, array(
                    'choices' => AudienceEnum::getAvailableTypes(),
                    'choices_as_values' => true,
                    'choice_label' => function($choice) {
                        return AudienceEnum::getTypeName($choice);
                    }));
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'OITBundle\Entity\ExpectedResult'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix() {
        return 'oitbundle_expectedresult';
    }

}
