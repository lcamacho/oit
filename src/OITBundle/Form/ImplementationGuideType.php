<?php

namespace OITBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use OITBundle\Enum\StatusEnum;

class ImplementationGuideType extends AbstractType {

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder->add('startDate')->add('endDate')->add('personInCharge')->add('email')
                //->add('status', ChoiceType::class, array(
                //    'choices' => StatusEnum::getAvailableTypes(),
                //    'choices_as_values' => true,
                //    'choice_label' => function($choice) {
                //        return StatusEnum::getTypeName($choice);
                //    }))
                    ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'OITBundle\Entity\ImplementationGuide'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix() {
        return 'oitbundle_implementationguide';
    }

}
