<?php

namespace OITBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use OITBundle\Entity\Activity;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use OITBundle\Entity\Allies;

class StrategicLineType extends AbstractType {

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder->add('objetive', EntityType::class, array(
                    'class' => 'OITBundle:Objetives'))
                ->add('line', EntityType::class, array(
                    'class' => 'OITBundle:Line'))
                ->add('action', EntityType::class, array(
                    'class' => 'OITBundle:Action'))
                ->add('activities', EntityType::class, array('class' => Activity::class, "expanded" => true, 'multiple' => true))
                ->add('startDate', DateType::class, array(
                    'widget' => 'single_text'))
                ->add('endDate', DateType::class, array(
                    'widget' => 'single_text'))
                ->add('verificationSources', TextareaType::class, array(
                    'attr' => array('rows' => '4')))
                ->add('media', TextareaType::class, array(
                    'attr' => array('rows' => '4')))
                ->add('budget', TextareaType::class, array(
                    'attr' => array('rows' => '4')))
                ->add('allies', EntityType::class, array('class' => Allies::class, "expanded" => true, 'multiple' => true));
        
        
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'OITBundle\Entity\StrategicLine'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix() {
        return 'oitbundle_strategicline';
    }

}
