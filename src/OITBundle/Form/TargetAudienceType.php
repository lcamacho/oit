<?php

namespace OITBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class TargetAudienceType extends AbstractType {

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder->add('audience')
                ->add('sector')
                ->add('numberOfMens')
                ->add('numberOfWomens')
                ->add('minAgeRange')
                ->add('maxAgeRange')
                ->add('profile', TextareaType::class, array(
                    'attr' => array('rows' => '4')))
                ->add('communicationSkills', TextareaType::class, array(
                    'attr' => array('rows' => '4')))
                ->add('vIHRiskLevel', ChoiceType::class, array(
                    'choices' => array(
                        'Muy bajo' => 1,
                        'Bajo' => 2,
                        'Medio' => 3,
                        'Alto' => 4,
                        'Muy alto' => 5,
                    ), "expanded" => true, 'multiple' => false))
                ->add('implementationGuide');
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'OITBundle\Entity\TargetAudience'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix() {
        return 'oitbundle_targetaudience';
    }

}
