<?php

namespace OITBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use OITBundle\Entity\Objetives;
use OITBundle\Entity\Principles;
use OITBundle\Entity\Task;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class TeamType extends AbstractType {

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {

        $builder->add('job', ChoiceType::class, array(
                    'choices' => array(
                        'Gerente de RRHH' => 1,
                        'Director/a RSE' => 2,
                        'Gerente de Salud y Seguridad Ocupacional' => 3,
                        'Responsable de Clínica' => 4,
                        'Oficial de Comunicación' => 5,
                        'Otro' => 6,
            )))
                ->add('fullName')
                ->add('phone')
                ->add('email')
                ->add('skype')
                ->add('experienceInHIV', TextareaType::class, array(
                    'attr' => array('rows' => '5')))
                ->add('formtiveNeeds', TextareaType::class, array(
                    'attr' => array('rows' => '5')))
                ->add('supportTeam', TextareaType::class, array(
                    'attr' => array('rows' => '5')))
                ->add('phrase', TextareaType::class, array(
                    'attr' => array('rows' => '5')))
                ->add('objetives', EntityType::class, array('class' => Objetives::class, "expanded" => true, 'multiple' => true))
                ->add('principles', EntityType::class, array('class' => Principles::class, "expanded" => true, 'multiple' => true))
                ->add('tasks', EntityType::class, array('class' => Task::class, "expanded" => true, 'multiple' => true));
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'OITBundle\Entity\Team'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix() {
        return 'oitbundle_team';
    }

}
