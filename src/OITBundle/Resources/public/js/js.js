// Select all links with hashes
$('a.recursos-link')
  // Remove links that don't actually link to anything
  .not('[href="#"]')
  .not('[href="#0"]')
  .click(function(event) {
    // On-page links
    if (
      location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') 
      && 
      location.hostname == this.hostname
    ) {
      // Figure out element to scroll to
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
      // Does a scroll target exist?
      if (target.length) {
        // Only prevent default if animation is actually gonna happen
        event.preventDefault();
        $('html, body').animate({
          scrollTop: target.offset().top -10
        }, 1500, function() {
          // Callback after animation
          // Must change focus!
          var $target = $(target);
          //$target.focus();
          if ($target.is(":focus")) { // Checking if the target was focused
            return false;
          } else {
            $target.attr('tabindex','-1'); // Adding tabindex for elements not focusable
            //$target.focus(); // Set focus again
          };
        });
      }
    }
  });

  $(function(){

  var slider = $('.bxslider').bxSlider({
    mode: 'horizontal',
    captions: true,
    // slideWidth: 600,
    pager: true,
    autoStart: false,
    infiniteLoop: false,
    pagerType: 'full',
    controls: false,
  });

  //  $('.next-s').click(function(){
  //   slider.goToNextSlide();
  // });

  //  $('.prev-s').click(function(){
  //   slider.goToPrevSlide();
  // });


});